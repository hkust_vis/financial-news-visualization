var dateConst=['2014-6-06 00:00:00', '2014-6-07 00:00:00', '2014-6-08 00:00:00', '2014-6-09 00:00:00']
var gapConst=1200;
db.mc1.ensureIndex({id:1, 'Timestamp':1})

function secondsToTime(secs)
{
    var hour = Math.floor(secs / (60 * 60));
   
    var divisor_for_minutes = secs % (60 * 60);
    var minute = Math.floor(divisor_for_minutes / 60);
 
    var divisor_for_seconds = divisor_for_minutes % 60;
    var second = Math.ceil(divisor_for_seconds);
   
    if (hour < 10) hour = '0' + hour;
    if (minute < 10) minute = '0' + minute;
    if (second < 10) second = '0' + second;
    return '' + hour + ':' + minute + ':' + second;
}
db.timeline.drop();
for(var i=0;i<dateConst.length-1;i++){
	print(i)
	var date=dateConst[i];
	var day=date.split(' ')[0];

	var timeLine=[]
	for(var j=0;j<85200;j+=1200){
		var istart=j, iend=j+1200;
		var start=day+' '+secondsToTime(istart);
		var end=day+' '+secondsToTime(iend);
		var oneDurCount=db.mc1.distinct('id', {
			'Timestamp':{$gt:start, $lt:end}
		});
		timeLine.push({
			value:oneDurCount.length,
			time:day+' '+secondsToTime(istart+600),
		})
	}

	db.timeline.insert(timeLine);
}
