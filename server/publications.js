var Collection={};
Collection.NYSE_USO= new Meteor.Collection('NYSE_USO')
Collection.oilPriceSentimentNews= new Meteor.Collection('oilPriceSentimentNews')
Collection.stopword= new Meteor.Collection('stopword')
Collection.oilImpactNews= new Meteor.Collection('oilImpactNews')
Collection.oilImpactNetwork= new Meteor.Collection('oilImpactNetwork')
Collection.oilImpactNetworkNodes=new Meteor.Collection('oilImpactNetworkNodes')
Collection.tempSentimentNews= new Meteor.Collection('tempSentimentNews')

Collection.EuroUnemploymentRate=new Meteor.Collection('EuroUnemploymentRate')
Collection.unemploymentSentimentNews= new Meteor.Collection('unemploymentSentimentNews')
Collection.unemploymentImpactNetwork= new Meteor.Collection('unemploymentImpactNetwork')
Collection.unemploymentImpactNetworkNodes=new Meteor.Collection('unemploymentImpactNetworkNodes')
Collection.tempUnemploymentSentimentNews=new Meteor.Collection('tempUnemploymentSentimentNews')

Meteor.publish('oilPrice',function(){
	return Collection.NYSE_USO.find()
})

Meteor.publish('unemploymentRate',function(){
	return Collection.EuroUnemploymentRate.find()
})

Meteor.publish('oilPriceSentimentNews',function(sentiment,date,impact,years){
	var date1=[]
	for(var i=0;i<years.length;i++){
	var temp=[new Date("Jan 1, "+years[i].toString()),new Date("Feb 1, "+years[i].toString()),new Date("Mar 1, "+years[i].toString()),
		new Date("Apr 1, "+years[i].toString()),new Date("May 1, "+years[i].toString()),new Date("Jun 1, "+years[i].toString()),
		new Date("July 1, "+years[i].toString()),new Date("Aug 1, "+years[i].toString()),new Date("Sep 1, "+years[i].toString()),
		new Date("Oct 1, "+years[i].toString()),new Date("Nov 1, "+years[i].toString()),new Date("Dec 1, "+years[i].toString()),new Date("Jan 1, "+(years[i]+1).toString())]
	date1=date1.concat(temp)
	}
	console.log(date1[date[0]])
	console.log(date1[date[1]])
	if (impact){
		return Collection.oilPriceSentimentNews.find({
		sentiment: 0,
		publicationDate: {$gte:date1[date[0]],$lt:date1[date[1]]},
		},{fields:{"publicationDate":1,"Title":1,'sentiment':1, 'Full text':1}})

	}else{
	if (sentiment){
	return Collection.oilPriceSentimentNews.find({
		sentiment: {$gt:0},
		publicationDate: {$gte:date1[date[0]],$lt:date1[date[1]]},
		},{fields:{"publicationDate":1,"Title":1,'sentiment':1, 'Full text':1}})
}else{
	return Collection.oilPriceSentimentNews.find({
		sentiment: {$lt:0},
		publicationDate: {$gte:date1[date[0]],$lt:date1[date[1]]},
		},{fields:{"publicationDate":1,"Title":1,'sentiment':1, 'Full text':1}})
}
}
})

Meteor.publish('unemploymentSentimentNews',function(sentiment,date,impact,years){
	var date1=[]
	for(var i=0;i<years.length;i++){
	var temp=[new Date("Jan 1, "+years[i].toString()),new Date("Feb 1, "+years[i].toString()),new Date("Mar 1, "+years[i].toString()),
		new Date("Apr 1, "+years[i].toString()),new Date("May 1, "+years[i].toString()),new Date("Jun 1, "+years[i].toString()),
		new Date("July 1, "+years[i].toString()),new Date("Aug 1, "+years[i].toString()),new Date("Sep 1, "+years[i].toString()),
		new Date("Oct 1, "+years[i].toString()),new Date("Nov 1, "+years[i].toString()),new Date("Dec 1, "+years[i].toString()),new Date("Jan 1, "+(years[i]+1).toString())]
	date1=date1.concat(temp)
	}
	if (impact){
		return Collection.unemploymentSentimentNews.find({
		sentiment: 0,
		publicationDate: {$gte:date1[date[0]],$lt:date1[date[1]]},
		},{fields:{"publicationDate":1,"Title":1,'sentiment':1, 'Full text':1}})

	}else{
	if (sentiment){
	return Collection.unemploymentSentimentNews.find({
		sentiment: {$gt:0},
		publicationDate: {$gte:date1[date[0]],$lt:date1[date[1]]},
		},{fields:{"publicationDate":1,"Title":1,'sentiment':1, 'Full text':1}})
}else{
	return Collection.unemploymentSentimentNews.find({
		sentiment: {$lt:0},
		publicationDate: {$gte:date1[date[0]],$lt:date1[date[1]]},
		},{fields:{"publicationDate":1,"Title":1,'sentiment':1, 'Full text':1}})
}
}
})

Meteor.publish('stopword',function(){
	return Collection.stopword.find()
})

Meteor.publish('oilImpactNews',function(){
	return Collection.oilImpactNews.find()
})

Meteor.publish('oilImpactNetwork', function(){
	return Collection.oilImpactNetwork.find()
})

Meteor.publish('unemploymentImpactNetworkNodes',function(){
	return Collection.unemploymentImpactNetworkNodes.find()
})

Meteor.publish('unemploymentImpactNetwork', function(){
	return Collection.unemploymentImpactNetwork.find()
})

Meteor.publish('oilImpactNetworkNodes',function(){
	return Collection.oilImpactNetworkNodes.find()
})

Meteor.publish('tempSentimentNews',function(){
	return Collection.tempSentimentNews.find({},{fields:{'sentiment':1,'publicationDate':1,'sentimentRequest':1,'requestWord':1}})
})

Meteor.publish('tempUnemploymentSentimentNews',function(){
	return Collection.tempUnemploymentSentimentNews.find({},{fields:{'sentiment':1,'publicationDate':1,'sentimentRequest':1,'requestWord':1}})
})





var exec = Npm.require('child_process').exec;
var Fiber = Npm.require('fibers');
var Future = Npm.require('fibers/future');

Meteor.methods({

  callPython: function(keyword) {
    var fut = new Future();
    console.log('hi')
    exec('python '+process.cwd().replace('.meteor/local/build/programs/','')+'/meteorExtractWords.py '+keyword, function (error, stdout, stderr) {
    	console.log(stdout)
    	console.log(error)
      // if you want to write to Mongo in this callback
      // you need to get yourself a Fiber
      new Fiber(function() {
        fut.return('Python was here');
      }).run();

    });
    return fut.wait();
  },
    callPythonUnemployment: function(keyword) {
    var fut = new Future();
    console.log(process.cwd())

    exec('python '+process.cwd().replace('.meteor/local/build/programs/','')+'/meteorUnEmploymentExtractWords.py '+keyword, function (error, stdout, stderr) {
    	console.log(stdout)
    	console.log(error)
      // if you want to write to Mongo in this callback
      // you need to get yourself a Fiber
      new Fiber(function() {
        fut.return('Python was here');
      }).run();

    });
    return fut.wait();
  }
});
