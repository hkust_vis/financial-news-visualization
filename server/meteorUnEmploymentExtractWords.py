# -*- coding: utf-8 -*- 
from pymongo import MongoClient
import pandas as pd
from datetime import datetime
import time
from time import mktime
import copy
import re
import operator
from collections import OrderedDict
from stop_words import get_stop_words
import sys


increaseList=['appreciate','benefit', 'boost', 'boosted', 'boosting', 'boosts', 'climbed', 'climbing', 'gained', 'growth', 'heating', 'helped', 
				'high', 'higher', 'hike', 'hike', 'hikes', 'hopes', 'improved', 'improving', 'increase', 'increased', 'increases', 
				'increasing', 'jump', 'jumped', 'jumps', 'positive', 'pushed', 'pushed', 'pushing', 'raised', 'raising', 'rebound',
				 'recover', 'recovered', 'recovering', 'rise', 'rises', 'rising', 'rose', 'soaring', 'soars', 'spike', 'spur', 'strong', 
				 'stronger', 'support', 'supported', 'supports', 'surge', 'surges', 'surging']

decreaseList=['bailout', 'collapse', 'collapsed', 'crash', 'decline', 'declines', 'declining', 'drop', 'dropped', 'fall', 'fallen', 
				'falling', 'falls', 'fell', 'low', 'lower', 'plummeted', 'plummeting', 'plunge', 'plunged', 'plunging', 'slide', 'slides', 
				'sliding', 'slow', 'slower', 'slowing', 'slump', 'tumbled', 'tumbling', 'weak']

impactList=['impact','benefiting','affect','effect','change','linked','effects','change','costs','benefits','reduce']

def clean_essay(string, lower=False):
        string = re.sub(r"\\t", " ", string)   
        string = re.sub(r"\\n", " ", string)   
        string = re.sub(r"\\r", " ", string)   
        string = re.sub(r"[^A-Za-z]", " ", string)  
        string = re.sub(r"\s{2,}", " ", string)
        if lower:
            string = string.lower()
        return string.strip()

keyword=''
for i,val in enumerate(sys.argv):
	if (i !=0):
		keyword=keyword+' '+sys.argv[i]
if keyword !='':
	client = MongoClient('localhost',3001)
	db = client.meteor
	regex=re.compile(".*"+keyword+".*",re.IGNORECASE)
	end = datetime(2015, 1, 1, 0, 0, 0, 0)
	cursor = db.unemploymentSentimentNews.find({"Full text" : {"$regex":regex},"publicationDate": {'$lt': end}},{'_id':0})
	print cursor.count()
	wordlist=[]
	documents=[]
	importList=[]
	for document in cursor:
		if document['Full text']:
			Document={}
			isImpact=False
			temp=clean_essay(document['Full text'].lower()).split(keyword)
			#print temp
			score=0
			for i, val in enumerate(temp[:-1]):
				toggle=1
				tmp=temp[i].split()
				#print tmp
				predictSentiment=False
				dummy=0
				if len(tmp)>0:
					if len(tmp)>1:
						if len(tmp)>2:
							wordlist.append(tmp[-3])
							#print tmp[-3]
							if tmp[-3] in increaseList:
								score+=1
								dummy+=1
							if tmp[-3] in decreaseList:
								score-=1
								dummy-=1
							if tmp[-3] =='not':
								toggle =toggle*-1


						wordlist.append(tmp[-2])
						#print tmp[-2]
						if tmp[-2] in increaseList:
							score+=1*toggle
							dummy+=1*toggle
						if tmp[-2] in decreaseList:
							score-=1*toggle
							dummy-=1*toggle
						if tmp[-2] =='not':
							toggle =toggle*-1

					wordlist.append(tmp[-1])
					#print tmp[-1]
					if tmp[-1] in increaseList:
						score+=1*toggle
						dummy+=1*toggle
					if tmp[-1] in decreaseList:
						score-=1*toggle
						dummy-=1*toggle
				toggle=1
				tmp=temp[i+1].split()
				if len(tmp)>0:
					wordlist.append(tmp[0])
					#print tmp[0]
					if tmp[0] in increaseList:
							score+=1
							dummy+=1
					if tmp[0] in decreaseList:
							score-=1
							dummy-=1
					if tmp[0] =='not':
						toggle=toggle*-1
					if len(tmp)>1:
						wordlist.append(tmp[1])
						#print tmp[1]
						if tmp[1] in increaseList:
								score+=1*toggle
								dummy+=1*toggle
						if tmp[1] in decreaseList:
								score-=1*toggle
								dummy-=1*toggle
						if tmp[1] =='not':
							toggle=toggle*-1
						if len(tmp)>2:
							wordlist.append(tmp[2])
							#print tmp[2]
							if tmp[2] in increaseList:
								score+=1*toggle
								dummy+=1*toggle
							if tmp[2] in decreaseList:
								score-=1*toggle
								dummy-=1*toggle

			if score>0:
				#print document['Title'].replace(u'\xa0', u'')
				#print 'increase'
				document['sentimentRequest']=score
				document['requestWord']=keyword
				importList.append(document)

			if score<0:
				#print document['Title'].replace(u'\xa0', u'')
				#print 'decrease'
				document['sentimentRequest']=score
				document['requestWord']=keyword
				importList.append(document)


			if ((score==0) and (isImpact)):
				document['sentimentRequest']=score
				document['requestWord']=keyword
				importList.append(document)
	db.tempUnemploymentSentimentNews.remove()
	result = db.tempUnemploymentSentimentNews.insert_many(importList)
	print result.inserted_ids

	exit(0)
	result={}
	for val in wordlist:
		if val in result:
			result[val]+=1
		else:
			result[val]=1
	result=OrderedDict(sorted(result.items(), key=lambda t: t[1]))
	removeList=['s','in','the','and','in','to','as','are','that','on','will',
			'for','is','a','could','if','u','since','said','crude','global',
			'even','may','year','world','benchmark','markets','new','york',
			'amid','current','average','also','t','barrel','energy','hit',
			'per','recent','us','brent','international','unemployment','volatility',
			'information','service','however','around','now','despite','remain',
			'due','continue','likely','can','environment','term','last','cent',
			'market','break','sustained','years','added','Ltd','another','still'
			'mr','qui','cours','est','offre','el','say','pc','total','MR','MS','BT',
			'Lately','LAGARDE','headline','EnQuest','Informa','DR','Al','re','just',
			'Dr','related','less','non','replied','quarter','need','rate','TO','responds',
			'points','Europe']
	stop_words = get_stop_words('english')
	for val in list(set(removeList) | set(stop_words)):
		if val in result:
			result.pop(val,None)
	for key in result:
		print str(key)+": "+str(result[key])
			

		#documents.append(temp)