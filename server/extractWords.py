# -*- coding: utf-8 -*- 
from pymongo import MongoClient
import pandas as pd
from datetime import datetime
import time
from time import mktime
import copy
import re
import operator
from collections import OrderedDict
from stop_words import get_stop_words


increaseList=['benefit', 'boost', 'boosted', 'boosting', 'boosts', 'climbed', 'climbing', 'gained', 'growth', 
				'heating', 'helped', 'high', 'higher','highest', 'hike', 'hike', 'hikes', 'improved', 'improving', 'increase', 
				'increased', 'increases', 'increasing', 'jump', 'jumped', 'jumps', 'positive', 'pushed', 'pushed', 
				'pushing', 'raised', 'raising', 'rebound', 'recover', 'recovered', 'recovering', 'rise', 'rises', 
				'rising', 'rose','risen', 'soaring', 'soars','soared', 'spike', 'spur', 'strong', 'stronger', 'support', 'supported', 
				'supports', 'surge', 'surges', 'surging']

decreaseList=['collapse', 'collapsed', 'crash','cut', 'decline', 'declines', 'declining', 'drop', 'dropped','dropping', 'fall', 'fallen',
			'falling', 'falls', 'fell', 'low', 'lower','lowest', 'plummeted', 'plummeting', 'plunge', 'plunged', 'plunging', 'slide',
			 'slides', 'sliding', 'slump','slower','slow','slowing','slowed', 'tumbled', 'tumbling', 'weak','reducing']

impactList=['impact','benefiting','affect','effect','change','linked','effects','change','costs','benefits']

def clean_essay(string, lower=False):
        string = re.sub(r"\\t", " ", string)   
        string = re.sub(r"\\n", " ", string)   
        string = re.sub(r"\\r", " ", string)   
        string = re.sub(r"[^A-Za-z]", " ", string)  
        string = re.sub(r"\s{2,}", " ", string)
        if lower:
            string = string.lower()
        return string.strip()

#keywordWord=['can','could','may','might','will','would','must','should']
keywordWord=['']
keywordList=[]
notForwardLooking=True
keyword='economic growth'
front=False
if front:
	for words in keywordWord:
		keywordList.append(keyword+' '+words)
		keywordList.append(keyword+'s '+words)
else:
	for words in keywordWord:
		keywordList.append(words+keyword+' ')
		#keywordList.append(words+keyword+'s ')	
client = MongoClient('localhost',3001)
db = client.meteor
wordlist=[]
doCounting=True
if doCounting:
	for keyword in keywordList:
		print keyword
		regex=re.compile(".*"+keyword+".*",re.IGNORECASE)
		end = datetime(2015, 1, 1, 0, 0, 0, 0)
		cursor = db.unemploymentNews.find({"Full text" : {"$regex":regex},"publicationDate": {'$lt': end}},{'_id':0})

		documents=[]
		importList=[]
		for document in cursor:
			if document['Full text']:
				Document={}
				isImpact=False
				temp=clean_essay(document['Full text'].lower()).split(keyword)
				#print temp
				score=0
				for i, val in enumerate(temp[:-1]):
					if notForwardLooking:
						tmp=temp[i].split()
						#print tmp
				
						if len(tmp)>0:
							if len(tmp)>1:
								if len(tmp)>2:
									wordlist.append(tmp[-3])
									#print tmp[-3]
									if tmp[-3] in increaseList:
										score+=1
										
									if tmp[-3] in decreaseList:
										score-=1
										
									if tmp[-3] == 'impact':
										isImpact=True


								wordlist.append(tmp[-2])
								#print tmp[-2]
								if tmp[-2] in increaseList:
									score+=1
									
								if tmp[-2] in decreaseList:
									score-=1
									
								if tmp[-2] == 'impact':
									isImpact=True

							wordlist.append(tmp[-1])
							#print tmp[-1]
							if tmp[-1] in increaseList:
								score+=1
								
							if tmp[-1] in decreaseList:
								score-=1
								
							if tmp[-1] == 'impact':
								isImpact=True

					tmp=temp[i+1].split()
					if len(tmp)>0:
						if len(tmp)>1:
							if notForwardLooking:
								if len(tmp)>2:
									wordlist.append(tmp[2])
									#print tmp[2]
									if tmp[2] in increaseList:
										score+=1
										
									if tmp[2] in decreaseList:
										score-=1
										
									if tmp[2] == 'impact':
										isImpact=True

							wordlist.append(tmp[1])
							#print tmp[1]
							if tmp[1] in increaseList:
									score+=1
									
							if tmp[1] in decreaseList:
									score-=1
									
							if tmp[1] == 'impact':
								isImpact=True

						wordlist.append(tmp[0])
						#print tmp[0]
						if tmp[0] in increaseList:
								score+=1
								
						if tmp[0] in decreaseList:
								score-=1
								
						if tmp[0] == 'impact':
							isImpact=True

				if score>0:
					#print document['Title'].replace(u'\xa0', u'')
					#print 'increase'
					document['sentiment']=score
					document['isImpact']=isImpact
					importList.append(document)

				if score<0:
					#print document['Title'].replace(u'\xa0', u'')
					#print 'decrease'
					document['sentiment']=score
					document['isImpact']=isImpact
					importList.append(document)


				if ((score==0) and (isImpact)):
					document['sentiment']=score
					document['isImpact']=isImpact
					importList.append(document)

#result = db.unemploymentSentimentNews.insert_many(importList)
#print result.inserted_ids

result={}
for val in wordlist:
	if val in result:
		result[val]+=1
	else:
		result[val]=1
result=OrderedDict(sorted(result.items(), key=lambda t: t[1]))
removeList=['s','in','the','and','in','to','as','are','that','on','will',
			'for','is','a','could','if','u','since','said','crude','global',
			'even','may','year','world','benchmark','markets','new','york',
			'amid','current','average','also','t','barrel','energy','hit',
			'per','recent','us','brent','international','oil','volatility',
			'information','service','however','around','now','despite','remain',
			'due','continue','likely','can','environment','term','last','cent',
			'june','market','break','sustained','years','sanctions','opec','russia','mr']
stop_words = get_stop_words('english')
for val in list(set(removeList) | set(stop_words)):
	if val in result:
		result.pop(val,None)
for key in result:
	print str(key)+": "+str(result[key])
		

	#documents.append(temp)