Template.mainview.dataProcessor = function() {

    var nodes = []
    var ret = {
        version: '1.0'
    }

    ret.tree2graph=function(root) {
        var graph={
        	nodes:[],
        	edges:[]
        }

        function recurse(name, node, depth) {
        	if(typeof depth ==='number'){
        		depth++;
        	}else{
        		depth=1;
        	}
            if (node.children){
				node.children.forEach(function(child) {
	                recurse(node.name, child, depth);
	            });
	            graph.nodes.push({
	            	name:node.name,
	            	depth:depth,
	            })
	            if(name){
	            	graph.edges.push({
		            	source:name,
		                target: node.name,
		                value: 1,
	            	})
	            }
            }else {
				graph.edges.push({
	            	source:name,
	                target: node.name,
	                value: 1,
	            });
	            graph.nodes.push({
	            	name:node.name,
	            	depth:depth
	            })
            }
        };

        recurse(null, root);

        function formatGraph(graph){
        	var nodesIdx={};
        	for(var i=0;i<graph.nodes.length;i++){
        		var temp=graph.nodes[i];
        		nodesIdx[temp.name]=i;
        	}
        	for(var i=0;i<graph.edges.length;i++){
        		var temp=graph.edges[i];
        		temp.source=nodesIdx[temp.source];
        		temp.target=nodesIdx[temp.target];
        	}
        	return graph;
        }   
        return formatGraph(graph);
    }



    return ret;
}
