// collections are declared here
//access from both server side and client side.

Collection={};

Collection.NYSE_USO= new Meteor.Collection('NYSE_USO')
Collection.oilPriceSentimentNews= new Meteor.Collection('oilPriceSentimentNews')
Collection.stopword= new Meteor.Collection('stopword')
Collection.oilImpactNews= new Meteor.Collection('oilImpactNews')
Collection.oilImpactNetwork= new Meteor.Collection('oilImpactNetwork')
Collection.oilImpactNetworkNodes=new Meteor.Collection('oilImpactNetworkNodes')
Collection.tempSentimentNews= new Meteor.Collection('tempSentimentNews')

Collection.EuroUnemploymentRate=new Meteor.Collection('EuroUnemploymentRate')
Collection.unemploymentSentimentNews= new Meteor.Collection('unemploymentSentimentNews')
Collection.unemploymentImpactNetwork= new Meteor.Collection('unemploymentImpactNetwork')
Collection.unemploymentImpactNetworkNodes=new Meteor.Collection('unemploymentImpactNetworkNodes')
Collection.tempUnemploymentSentimentNews=new Meteor.Collection('tempUnemploymentSentimentNews')