$(document).ready(function() {
    $('[data-toggle="offcanvas"]').click(function() {
        $('.row-offcanvas').toggleClass('active')
    });
});



Router.configure({
    loadingTemplate: 'loading',
});

Router.route('home', {
  path: '/',
  layoutTemplate: 'layout',
  yieldTemplates: {
    'mainview': {to: 'view'},
  }
});

Router.route('part2', {
   path: '/part2',
 layoutTemplate: 'layout',
yieldTemplates: {
  'part2view': {to: 'view'},
 }
 });
/*
Router.map(function() {
    return this.route('home', {
        path: '/',
        waitOn: function() {
             return Meteor.subscribe('oilPriceSentimentNews',false,[0,12],false);
        },
        action: function() {
            this.render('layout');
        },


    })
});

*/

