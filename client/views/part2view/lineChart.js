LineChart=function(priceData,width,height) {
	width=width*0.85;
	height=height*0.85;
	var price=[];
	for(var i=0;i<priceData.length;i++){
		priceData[i]=parser(priceData[i])
		price.push(priceData[i]['Close'])
	}
	var format = d3.time.format("%d-%m-%Y");
    var margin = {top: height*0.1, right: width*0.2, bottom: height*0.2, left: width*0.1};


    var minDate = priceData[0].Date;
    var maxDate = priceData[priceData.length - 1].Date;
    // Set up time based x axis
    var x = d3.time.scale()
	  .domain([minDate, maxDate])
	  .range([0, width]);

    var y = d3.scale.linear()
	  .domain([getMinOfArray(price)*0.8,getMaxOfArray(price)*1.05 ])
	  .range([height, 0]);

    var xAxis = d3.svg.axis()
	  .scale(x)
	  .ticks(10)
	  .orient("bottom")
	  .innerTickSize(-height)
    	.outerTickSize(0)
	  .tickFormat(d3.time.format("%b"));

    var yAxis = d3.svg.axis()
	  .scale(y)
	  .ticks(7)
	  .orient("left");

    // put the graph in the "miles" div
    var svg = d3.select("#unemployment").append("svg")
	  .attr("width", width + margin.left + margin.right)
	  .attr("height", height + margin.top + margin.bottom)
	  .attr('id','lineChart')
	  .append("g")
	  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // function to draw the line
    var line = d3.svg.line()
	.x(function(d) { return x(d.Date); } )
	.y(function(d) { return y(d.Close); } );

    //Mouseover tip
    var tip = d3.tip()
	.attr('class', 'd3-tip')
	.offset([120, 40])
	.html(function(d) {
	    return 'Date: '+format(d.Date) + "<br>"+"Value: " + d.Close +
                "<br>";
	});

    svg.call(tip);

    // add the x axis and x-label
    svg.append("g")
	  .attr("class", "x axis")
	  .attr("transform", "translate(0," + height + ")")
	  .call(xAxis)
	  .selectAll("text")
	  .attr("y", 5)
	  .attr("x", 9)
	  .attr("dy", ".35em")
	  .attr("transform", "rotate(30)")
	  .style("text-anchor", "start");


    // add the y axis and y-label
    svg.append("g")
	  .attr("class", "y axis")
	  .attr("transform", "translate(0,0)")
	  .call(yAxis);
	  /*
    svg.append("text")
	  .attr("class", "ylabel")
	  .attr("y", 0 - margin.left*0.5) // x and y switched due to rotation!!
	  .attr("x", 0 - (height / 2))
	  .attr("dy", "1em")
	  .attr("transform", "rotate(-90)")
	  .style("text-anchor", "middle")
	  .text("Unemployment Rate");
*/
/*
    svg.append("text")
	  .attr("class", "h3")
	  .attr("y", 0)
	  .attr("x", width/2)
	  .style("text-anchor", "middle")
	  .text("OIL CLOSE PRICE");
*/
	  var month=Session.get('newsSelectedMonth')
  	var years=Session.get('lineChartDisplayYear')
    // draw the line
    svg.append("path")
	  .attr("d", line(priceData));
    svg.selectAll(".dot")
	  .data(priceData)
	  .enter().append("rect")
	  .attr('class', 'datapoint')
	  .attr('x', function(d) { return x(d.Date); })
	  .attr('y', function(d) { return y(d.Close); })
	  .attr('width', function(d){
	  	if((d.Date.getMonth()+12*years.indexOf(d.Date.getFullYear()))===month){
	  	return 4
	  }
	  	return 1
	})
	  .attr('height', function(d){
	  	if((d.Date.getMonth()+12*years.indexOf(d.Date.getFullYear()))===month){
	  	return 4
	  }
	  	return 1
	})
	  .attr('fill', function(d){
	  	if((d.Date.getMonth()+12*years.indexOf(d.Date.getFullYear()))===month){
	  	return 'red'
	  }
	  	return'#00FFFF'
	})
	  .on('mouseover', tip.show)
	  .on('mouseout', tip.hide);
	  
	  function parser(d) {
    d.Date = new Date(d.Date);
    return d;
}
function getMaxOfArray(numArray) {
  return Math.max.apply(null, numArray);
}
function getMinOfArray(numArray) {
  return Math.min.apply(null, numArray);
}
}