scatterPlot=function(data,width,height,requestWord,drawLine,id,name) {

var margin = {top: height*0.05, right: width*0.05, bottom: height*0.05, left:width*0.05};
    width = width - margin.left - margin.right;
    height = height - margin.top - margin.bottom;

/* 
 * value accessor - returns the value to encode for a given data object.
 * scale - maps value to a visual display encoding, such as a pixel position.
 * map function - maps from data value to display value
 * axis - sets up axis
 */ 


// setup x 

var xValue = function(d) { 
  if((d.sentimentRequest<10)||(d.sentimentRequest>-10)){
return d.sentimentRequest;
}else if(d.sentimentRequest>10){

  return 10;
}else{
  return -10;
}
}, // data -> value
    xScale = d3.scale.linear().range([0, width]), // value -> display
    xMap = function(d) {
    return xScale(xValue(d));}, // data -> display
    xAxis = d3.svg.axis().scale(xScale).orient("bottom").ticks(5);

// setup y
var yValue = function(d) { 
  if((d.sentiment<10)||(d.sentiment>-10)){
return d.sentiment;
}else if(d.sentiment>10){

  return 10;
}else{
  return -10;
}
}, // data -> value
    yScale = d3.scale.linear().range([height, 0]), // value -> display
    yMap = function(d) { return yScale(yValue(d));}, // data -> display
    yAxis = d3.svg.axis().scale(yScale).orient("left").ticks(5);

// setup fill color
var cValue = function(d) { return d.month;},
    color = d3.scale.category20();

// add the graph canvas to the body of the webpage
var svg = d3.select(id);
//console.log(svg)
svg.selectAll("*").remove();

var svg = svg
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// add the tooltip area to the webpage
var tooltip = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

// load data


/*
  // change string (from CSV) into number format
  data.forEach(function(d) {
    d.Calories = +d.Calories;
    d["Protein (g)"] = +d["Protein (g)"];
//    console.log(d);
  });
*/

  // don't want dots overlapping axis, so add in buffer to data domain
  //xScale.domain([d3.min(data, xValue), d3.max(data, xValue)]);
 // yScale.domain([d3.min(data, yValue), d3.max(data, yValue)]);
 xScale.domain([-10, 10]);
  yScale.domain([-10, 10]);

var distanceX=0
distanceX=xScale(0)-xScale(-10)
/*
data.sort(function(a, b) {
  return xMap(a) - xMap(b);
});


for(var i=0;i<data.length;i++){
  if(data[i]['sentimentRequest']===0){
    distanceX=xMap(data[i])-xMap(data[0])
  }
}
*/
var distanceY=0
distanceY=yScale(-10)-yScale(0)
//console.log(distanceY)
/*
data.sort(function(a, b) {
  return yMap(a) - yMap(b);
});

for(var i=0;i<data.length;i++){
  if(data[i]['sentiment']===0){
    distanceY=yMap(data[i])-yMap(data[0])
  }
}
*/
  // x-axis
  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + distanceY + ")")
      .call(xAxis)
    .append("text")
      .attr("class", "label")
      .attr("x", width)
      .attr("y", -6)
      .style("text-anchor", "end")
      .text(requestWord);



  // y-axis
  svg.append("g")
      .attr("class", "y axis")
       .attr("transform", "translate("+distanceX+", 0 )")
      .call(yAxis)
    .append("text")
      .attr("class", "label")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text(name);

/*
if(data.length!==0){
data.sort(function(a, b) {
  return a['publicationDate'].getMonth()- b['publicationDate'].getMonth();
});
}
*/
if(data.length!==0){
data.sort(function(a, b) {
  return a['size']- b['size'];
});
}
var years=Session.get('lineChartDisplayYear');
var colors=[]
for(var i=0;i<years.length;i++){
var color={'Jan':'#a6cee3', 'Feb':'#1f78b4', 'Mar':'#b2df8a', 'Apr':'#33a02c', 'May':'#fb9a99', 'Jun':'#e31a1c', 
            'Jul':'#fdbf6f', 'Aug':'#ff7f00', 'Sep':'#cab2d6', 'Oct':'#6a3d9a', 'Nov':'#bdbdbd', 'Dec':'#b15928'}
var dummy=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
for (val in color) {
  var newName=val+' '+years[i].toString()
   color[newName]=color[val]
  delete color[val];
}
colors.push(color)
}
//console.log(data)
//console.log(colors)
  // draw dots
  svg.selectAll(".dot")
      .data(data)
    .enter().append("circle")
      .attr("class", "dot")
      .attr("r", function(d){return (width/200)*Math.log(d.size+1)})
      .attr("cx", xMap)
      .attr("cy", yMap)
      .attr('opacity',1)
      .style("stroke", function(d) {
        for(var i=0;i<colors.length;i++){
        if(d.month in colors[i]){ 
        return colors[i][d.month];
      }
      }
      }) 
      .style('stroke-width',1)
      .style('fill',function(d) { 
        for(var i=0;i<colors.length;i++){
        if(d.month in colors[i]) {
        return colors[i][d.month];
      }
      }
      })
      .on("mouseover", function(d) {
          tooltip.transition()
               .duration(200)
               .style("opacity", .9);
          tooltip.html(d["month"] + "<br/> (" + xValue(d) 
	        + ", " + yValue(d) + ")")
               .style("left", (d3.event.pageX + 5) + "px")
               .style("top", (d3.event.pageY - 28) + "px");
      })
      .on("mouseout", function(d) {
          tooltip.transition()
               .duration(500)
               .style("opacity", 0);
      });

  // draw legend
  
var color_domain=[]
for(var j=0;j<colors.length;j++){
for(i in colors[j]){
  color_domain.push(i)
}
}
  var legend = svg.selectAll(".legend")
      .data(color_domain)
    .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) { return "translate(" + i * (width/50) + ",0)"; });
/*
if(drawLine.length!==0){
      // apply the reults of the least squares regression
    var x1 = drawLine[0][0];
    var y1 = drawLine[0][1];
    var x2 = drawLine[1][0];
    var y2 = drawLine[1][1];
    var trendData = [[x1,y1,x2,y2]];
    
    var trendline = svg.selectAll(".trendline")
      .data(trendData);
      
    trendline.enter()
      .append("line")
      .attr("class", "trendline")
      .attr("x1", function(d) { return xScale(d[0]); })
      .attr("y1", function(d) { return yScale(d[1]); })
      .attr("x2", function(d) { return xScale(d[2]); })
      .attr("y2", function(d) { return yScale(d[3]); })
      .attr("stroke", "black")
      .attr("stroke-width", 1);
}
*/
  // draw legend colored rectangles
  legend.append("rect")
      .attr("x", function(d,i){return width*0.8-12*parseInt(i/12)* (width/50)})
      .attr('y',function(d,i){return height-parseInt(i/12)* (width/50)})
      .attr("width", height/25)
      .attr("height", height/25)
      .on('mouseover',function(d){
        svg.selectAll(".dot").style('opacity',function(k){
          if(k.month!==d) {
            return 0
          }else{
            return 1
          }
        });
        legend.selectAll("text").style('opacity',function(k){
          if(k!==d) {
            return 0
          }else{
            return 1
          }
        });
      })
      .style("fill", function(d){
       for(var i=0;i<colors.length;i++){
        if(d in colors[i]){
        return colors[i][d]
      }
      }
      })
      
      .on('mouseout',function(d){
        svg.selectAll(".dot").style('opacity',1)
      });

  // draw legend text
  legend.append("text")
      .attr("x", function(d, i){return (width*0.79-(i%12) * (width/50)-12*parseInt(i/12)* (width/50))})
      .attr("y", function(d,i){return height*1.04-parseInt(i/12)* (width/50)})
      .style("text-anchor", "end")
      .style('opacity',0)
      .text(function(d) { return d;})

}