d3.selection.prototype.moveToFront = function() {
  return this.each(function(){
  this.parentNode.appendChild(this);
  });
};

GraphVis=function(selector,links,width,height) {
  //this.nodes = nodes;
  //this.links = links;
  // init svg
  var svg = d3.select(selector);
  // clean up all previous items before render
  svg.selectAll("*").remove();
  // wrap graph into container to enable zoom and pan
  var container = svg.append('g');
  // zoom (and accompanying pan)
  var zoom = d3.behavior.zoom()
    .scaleExtent([0, 5])
    .on("zoom", function() {
      container.attr("transform",
        "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    });
  svg.call(zoom);


var nodes = {};

// Compute the distinct nodes from the links.
links.forEach(function(link) {
  link.source = nodes[link.source] || (nodes[link.source] = {name: link.source});
  link.target = nodes[link.target] || (nodes[link.target] = {name: link.target});
});

nodes['unemployment']['fixed']=true
nodes['unemployment']['x']=width/2
nodes['unemployment']['y']=height/2
var force = d3.layout.force()
    .nodes(d3.values(nodes))
    .links(links)
    .size([width, height])
    .linkStrength(function(d){
      return d.linkStength
    })
    .linkDistance(width/3)
    .charge(-width/4)
    .on("tick", tick)

svg
    .attr("width", width)
    .attr("height", height);

var link=container.append('g').selectAll("path"),
labelLine=container.append('g').selectAll("path"),
node=container.append('g').selectAll(".node")

    


function tick() {
  link
      .attr("x1", function(d) { return d.source.x; })
      .attr("y1", function(d) { return d.source.y; })
      .attr("x2", function(d) { return d.target.x; })
      .attr("y2", function(d) { return d.target.y; });
    
  labelLine
      .attr("x", function(d) {
          return (d.source.x + d.target.x)/2; })
      .attr("y", function(d) {
          return (d.source.y + d.target.y)/2; });
        
  
  node
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
}

function mouseover(d) {
  console.log(minStrength)
  var targets=[]
  var strength=[]
  link.style('stroke-width', function(l) {
            if (d === l.source || d === l.target){
              if(d===l.source){
                if(targets.indexOf(l.target)===-1){
              targets.push(l.target.name)
              strength.push(l.linkStength)
            }
            }else{
               if(targets.indexOf(l.source)===-1){
              targets.push(l.source.name)
              strength.push(l.linkStength)
            }
            }
            return 2;
}
          else
            return 0;
        });

  
  node.selectAll('text')
    .attr("opacity",function(d){
      if(targets.indexOf(d.name)!==-1){
        if(strength[targets.indexOf(d.name)]>minStrength){
          console.log( strength[targets.indexOf(d.name)])
        return 1
      }else return 0
      }else{
        return 0
      }
    })

  d3.select(this).select("circle").transition()
      .attr("r", width/50);
  d3.select(this).select('text').transition()
      .attr("opacity",1)
      .style("font", "normal "+(width/20).toString()+"px Arial")
      .style("fill",'white')
      .style("stroke-width", ".5px")
  d3.select(this).moveToFront();
}

function mouseout(d) {
  link.style('stroke-width',1)
  node.selectAll('text')
    .attr("opacity",0)
  d3.select(this).select("circle").transition()
      .attr("r", function(d){
      if(d.name!=='oil price'){
      return width/100
    }else{
      return width/50
    }
    });
    d3.select(this).select('text').transition()
      .style("font", "normal "+(width/40).toString()+"px Arial")
      .attr("opacity",function(d){
      if(d.name!=='oil price'){
      return 0
    }else{
      return 1
    }
    })
}
  

  // resize svg and force layout when screen size change
  function resize() {
    var width = 600//window.innerWidth,
      height =600// window.innerHeight;
    svg.attr("width", width).attr("height", height);
    force.size([width, height]).resume();
  }

  this.render = function() {
 link = link
    .data(force.links())
  .enter()
    .append("g")
    .attr("class", "link")
    .append("line")
    .style('stroke-width',0)
    .attr('opacity',function(d) {
        return d.linkStength;
      })
    .attr("class", "link-line")
    .attr("marker-end", function(d) {
        return "url(#end)";
      });

 node = node
    .data(force.nodes())
  .enter().append("g")
    .attr('id',function(d){
      return d.name})
    .attr("class", function(d){
      if(d.name==='unemployment'){
                  return 'node-main'
                }else {return 'node'}})
    .on("mouseover", mouseover)
    .on("mouseout", mouseout)
    .on('click',function(d){
      if(d.name!=='unemployment'){
      $('#lda1').multiselect('select', [d.name]);
      list=Session.get('lda1')
      if(list.indexOf(d.name)!==-1){
        d3.select(this).attr('class','node')
        list.splice(list.indexOf(d.name, 1));
        Session.set('lda1',list)
       // console.log(list)
      }else{
        d3.select(this).attr('class','node-selected')
        list.push(d.name)
        Session.set('lda1',list)
       // console.log(list)
      }
    }
    })
    .call(force.drag);


node.append("circle")
    .attr("r", function(d){
      if(d.name==='unemployment'){
                  return width/50
                }else return width/100});

node.append("text")
    .attr("dx", 12)
    .attr("dy", ".35em")
    .style("font", "normal "+(width/40).toString()+"px Arial")
    .attr("opacity",function(d){
      if(d.name!=='unemployment'){
      return 0
    }else{
      return 1
    }
    })
    .style("fill",'white')
    .text(function(d) { return d.name; });

/*
 labelLine = labelLine
    .append("text")
    .attr("class", "link-label")
   .attr("font-family", "Arial, Helvetica, sans-serif")
    .attr("fill", "Black")
    .style("font", "normal 12px Arial")
    .attr("dy", ".35em")
    .attr("text-anchor", "middle")
*/

    force.start();
    force.size([width, height]).resume();
  }

  }
