Template.mainview.rendered = function() {
document.getElementById('mainContainer').setAttribute('style','height:'+($('#forceGraphContainer').width()*3.3).toString()+'px;');
  $('.selectpicker').selectpicker();
   $('#textIndicator').on('keypress', function (event) {
         if(event.which === 13){

            //Disable textbox to prevent multiple submit
            //$(this).attr("disabled", "disabled");
            //Session.set('scatterPlotLegend',$('#textIndicator').val())
            Meteor.call('callPython',$('#textIndicator').val())
            Session.set('requestWord',$('#textIndicator').val())
            //Do Stuff, submit, etc..
         }
   });

for(var i=2;i<11;i++){
  (function(j) {
  if($('#lda'+j.toString()).length!==0){
    $('#lda'+j.toString()).multiselect({
      buttonText: function(options, select) {
        return 'Select Topics';
      },
      onChange: function(option, checked, select) {
       // console.log(j)
        var list=Session.get('lda'+j.toString())

                if(checked){
                  list.push($(option).val())
                }else{
                  list.splice(list.indexOf($(option).val()), 1);
                }
                Session.set('lda'+j.toString(),list)

              }  
            });
  }

  })(i)
 
}

  document.getElementById('newsTableBody').setAttribute('height',$('#newsContainer').width()/2);



//Gromit
  var donutChart1 = c3.generate({
    bindto: '#donutChart1',
    size: {
    height: $("#donutChart1").width(),
    width: $("#donutChart1").width()
  },
  tooltip: {
    format: {
        value: function (value, ratio, id) {
            return value;
        }
    }
},
    data: {
      onclick: function(d,i){
      var list=Session.get('ldaList')
      var result=[]
      for(var i=0;i<list.length;i++){
        if(list[i][0].split(',').indexOf(d.id)!==-1){
          result.push(list[i][0])
        }
      }
      Session.set('lda3',result)
      },
        columns: [
        ],
        type : 'donut',
    },
    donut: {
        title: "Topics Occurance",
        label: {
      format: function (value) { return value; }
    }
    },
    legend: {
        show: false
    }
});

  var donutChart2 = c3.generate({
    bindto: '#donutChart2',
    size: {
    height: $("#donutChart2").width(),
    width: $("#donutChart2").width()
  },
  tooltip: {
    format: {
        value: function (value, ratio, id) {
            return value;
        }
    }
},
    data: {
        columns: [

        ],
        type : 'donut',
        onclick: function (d, i) { 
        //  console.log("onclick", d.id);
          Session.set('lda2',[d.id])
         }
    },
    donut: {
        title: "Topics News Retrival",
        label: {
      format: function (value) { return value; }
    }
    },
    legend: {
        show: false
    }
});
var sentimentChart = c3.generate({
  bindto: '#oilPriceSentiment',
  size: {
    height: $("#oilPriceSentiment").width()/4,
    width: $("#oilPriceSentiment").width()
  },

  data: {
    x: 'x',
    onclick: function (d, i) { 
      Session.set('newsSelectedMonth',d.x);
    },
    columns: [
    ['x', 0,1, 2, 3, 4, 5, 6, 7, 8, 9,10,11],
      //  ['data1'],
       // ['data2', 50, 20, 10, 40, 15, 25]
       ],
       groups: [
       ['negativeNews', 'positiveNews']
       ],
       colors: {
     //   price: '#2b83ba',
     negativeNews: '#fc8d59',
     positiveNews: '#99d594'
   },
   axes: {
    negativeNews: 'y',
    positiveNews: 'y'
  },
  types: {
    //    price: 'spline',
    negativeNews: 'bar',
    positiveNews: 'bar'
  }
},
axis: {
  x: {
   // categories:['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    type: 'category'
  },
  y: {
    show: true

  }
}
});

$("#slider").noUiSlider({
        start: 0,
        step:0.1,
        range: {
            'min': 0,
            'max': 1
        }
    });

  Deps.autorun(function(){
    var years=Session.get('lineChartDisplayYear')
    var subscribe_handler=Meteor.subscribe('tempSentimentNews')
    if(subscribe_handler.ready()){
    var data=Collection.tempSentimentNews.find().fetch()
    categories=[]
    for(var i=0;i<years.length;i++){
    var temp=['Jan '+years[i].toString(), 'Feb '+years[i].toString(),
     'Mar '+years[i].toString(), 'Apr '+years[i].toString(),
      'May '+years[i].toString(), 'Jun '+years[i].toString(),
       'Jul '+years[i].toString(), 'Aug '+years[i].toString(), 
       'Sep '+years[i].toString(), 'Oct '+years[i].toString(),
        'Nov '+years[i].toString(), 'Dec '+years[i].toString()]
        categories=categories.concat(temp)
  }
    for(var i=0;i<data.length;i++){
      data[i]['month']=categories[data[i]['publicationDate'].getMonth()*(years.indexOf(data[i]['publicationDate'].getFullYear())+1)]
    }
    
    if(data.length!==0){
     // console.log(data)
      var Data=[]
      var ref={}
      var x=[]
      var y=[]
      //findLineByLeastSquares(x,y)
      for(var i=0;i<data.length;i++){
        y.push(data[i]['sentiment'])
        x.push(data[i]['sentimentRequest'])
        var tmp=data[i]['sentiment'].toString()+','+data[i]['sentimentRequest'].toString()+','+data[i]['publicationDate'].getMonth().toString()
        if(tmp in ref){
          ref[tmp]['size']+=1
        }else{
          ref[tmp]=data[i]
          ref[tmp]['size']=1
        }
      }
      
      /*
      var leastSquareTmp=findLineByLeastSquares(x,y)
      var leastSquare=[]
      for(var i=0;i<leastSquareTmp[0].length;i++){
        var Tmp=[]
        Tmp.push(leastSquareTmp[0][i])
        Tmp.push(leastSquareTmp[1][i])
        leastSquare.push(Tmp)
      }
      leastSquare.sort(function(a,b){
        return a[0]-b[0]
      })
      var drawLine=[leastSquare[0],leastSquare[leastSquare.length-1]]
      */

      for(val in ref){
        Data.push(ref[val])
      }
    scatterPlot(Data,$('#scatterPlotContainer').width(),$('#scatterPlotContainer').width()/2,data[0]['requestWord'],[],"#scatterPlotVis",'Oil Price')
  }else{

    scatterPlot(data,$('#scatterPlotContainer').width(),$('#scatterPlotContainer').width()/2,Session.get('requestWord'),[],"#scatterPlotVis",'Oil Price')
  }
  }
  
  });

    Deps.autorun(function(){
      var ldaData=[]
      var subscribe_handler=Meteor.subscribe('oilImpactNetworkNodes')
      if(subscribe_handler.ready()){
        ldaData=Collection.oilImpactNetworkNodes.find().fetch();
        var ldaList=''
        var sortingList={}
        for(var i=0;i<ldaData.length;i++){
          var tmp=ldaData[i]['name'].split(',')
          for(var j=0;j<tmp.length;j++){
            if(tmp[j] in sortingList){
              sortingList[tmp[j]]+=1
            }else{
              sortingList[tmp[j]]=1
            }
          }
        }
        var donutData=getSortedKeys(sortingList)
        var donutList=[]
        for(var i=0;i<20;i++){
          var tmp=[donutData[i],sortingList[donutData[i]]]
          donutList.push(tmp)
        }
        donutChart1.load({
        columns: donutList
    });
        var sortResult=mergeSort(ldaData,sortingList)
        var saveList=[]

        for(var i=0;i<sortResult.length;i++){
        saveList.push([sortResult[i]['name'],sortResult[i]['documents'].length])
   ldaList+='<option value="'+sortResult[i]['name']+'">'+sortResult[i]['name']+'</option>'
 }
 saveList.sort(function(a, b) {
  return b[1] - a[1];
});
 Session.set('ldaList',saveList)
 var donutList=[]
        for(var i=0;i<20;i++){
          var tmp=[saveList[i][0],saveList[i][1]]
          donutList.push(tmp)
        }
        donutChart2.load({
        columns: donutList
    });
 for(var i=2;i<11;i++){
  $('#lda'+i.toString()).append(ldaList);
  $('#lda'+i.toString()).multiselect('rebuild');
}
}
});




    Deps.autorun(function(){
    var year=Session.get('lineChartDisplayYear')
    var monthList=[]
    for(var i=0;i<year.length;i++){
    var MonthList=[[new Date('January 1, '+year[i].toString()),new Date('January 31, '+year[i].toString())],
    [new Date('Feb 1, '+year[i].toString()),new Date('Feb 28, '+year[i].toString())],
    [new Date('March 1, '+year[i].toString()),new Date('March 31, '+year[i].toString())],
    [new Date('April 1, '+year[i].toString()),new Date('April 30, '+year[i].toString())],
    [new Date('May 1, '+year[i].toString()),new Date('May 31, '+year[i].toString())],
    [new Date('June 1, '+year[i].toString()),new Date('June 30, '+year[i].toString())],
    [new Date('July 1, '+year[i].toString()),new Date('July 31, '+year[i].toString())],
    [new Date('August 1, '+year[i].toString()),new Date('August 31, '+year[i].toString())],
    [new Date('September 1, '+year[i].toString()),new Date('September 30, '+year[i].toString())],
    [new Date('October 1, '+year[i].toString()),new Date('October 31, '+year[i].toString())],
    [new Date('November 1, '+year[i].toString()),new Date('November 30, '+year[i].toString())],
    [new Date('December 1, '+year[i].toString()),new Date('December 31, '+year[i].toString())]
    ]
    monthList=monthList.concat(MonthList)
  }
      var taskNames = [ "", "Force","Retrival",'Occur.'];
      var list=[Session.get('lda1'),Session.get('lda2'),Session.get('lda3')];
      var ldaData=[[]]
      for(var i=4;i<11;i++){
        taskNames.push('In. '+i.toString())
        list.push(Session.get('lda'+i.toString()))
        ldaData.push([])
      }
      var subscribe_handler=Meteor.subscribe('oilImpactNetworkNodes')
      var tasks=[]
      if(subscribe_handler.ready()){
        for(var k=0;k<10;k++){
          var nameTotal=''
          if(list[k].length>0){
            var query=[]
            for(var i=0;i<list[k].length;i++){
            nameTotal+=list[k][i]+' '
             var  tmp={}
             tmp['name']=list[k][i]
             query.push(tmp)
           }
           ldaData[k]=Collection.oilImpactNetworkNodes.find({$or:query}).fetch();
    var sentimentList={0:[],1:[],2:[],3:[],4:[],5:[],6:[],7:[],8:[],9:[],10:[],11:[]};
    for(var i=0;i<ldaData[k].length;i++){
      for(var j=0;j<ldaData[k][i]['documents'].length;j++){
        sentimentList[ldaData[k][i]['documents'][j].date.getMonth()].push(ldaData[k][i]['documents'][j].sentiment)
      }
    }

    for(month in sentimentList){
      var tmp={}
      if(sentimentList[month].length>0){
        var sentiment=0
        for(var i=0;i<sentimentList[month].length;i++){
          sentiment+=sentimentList[month][i]
        }
        sentiment=parseFloat((parseFloat(sentiment)/parseFloat(sentimentList[month].length)))
        tmp['name']=nameTotal
        tmp['startDate']=monthList[month][0]
        tmp['endDate']=monthList[month][1]
        tmp['taskName']=taskNames[k+1]
        tmp['sentiment']=sentiment
        if (sentiment>0){
          tmp['status']='SUCCEEDED'
        }else if(sentiment===0){
          tmp['status']='RUNNING'
        }else{
          tmp['status']='FAILED'
        }
        tasks.push(tmp)
      }
    }


  }
}
var taskStatus = {
  "SUCCEEDED" : "bar",
  "FAILED" : "bar-failed",
  "RUNNING" : "bar-running",
  "KILLED" : "bar-killed"
};

tasks.sort(function(a, b) {
  return a.endDate - b.endDate;
});
var maxDate = monthList[monthList.length-1][1];
tasks.sort(function(a, b) {
  return a.startDate - b.startDate;
});
var minDate =monthList[0][0];

var format = "%m-%d-%y";

if($('#ganttChart').length!==0){
  remove('ganttChart')
}
var gantt = d3.ganttOil(minDate,maxDate).taskTypes(taskNames).taskStatus(taskStatus).tickFormat(format);
gantt(tasks);
document.getElementById('oilPriceGantt').setAttribute('style','position:absolute; top:'+(($('#priceChartContainer').width()/2)).toString()+'px;');


}
})
Deps.autorun(function(){
 var bool=Session.get('boolNews')
 if(bool){
  for(var i=0;i<5;i++){
    if($('#positive'+i.toString()).length!==0){
      eventFire(document.querySelector('#positive'+i.toString()),'popover');
    }
  }
}else{
  for(var i=0;i<5;i++){
    if($('#negative'+i.toString()).length!==0){
      eventFire(document.querySelector('#negative'+i.toString()),'popover');
    }
  }
}
});

Deps.autorun(function () {
  var years=Session.get('lineChartDisplayYear')
  var lineChartData=[];
  var subscribe_handler=Meteor.subscribe('oilPrice');
  if (subscribe_handler.ready()) {
    lineChartData = Collection.NYSE_USO.find({
    }).fetch();

    lineChartData.sort(function(a,b){
      return  a.Date -  b.Date;
    })
    minYear=lineChartData[0].Date.getFullYear()
    maxYear=lineChartData[lineChartData.length-1].Date.getFullYear()
    var wholeYear=[]
    wholeYear.push(minYear)
    for(var i=0;i<(maxYear-minYear);i++){
      wholeYear.push(minYear+1)
    }
    Session.set('lineChartDisplayYear',wholeYear)
    var data=[]
    var date=[]
    for(var i=0;i<lineChartData.length;i++){
      date.push(moment(lineChartData[i].Date).format('MM-DD-YYYY'))
      data.push(lineChartData[i].Close)
    }
    var input=[]
    var closeFirst=lineChartData[0]['Close'];
    var closeLast=lineChartData[lineChartData.length-1]['Close'];
    for (var i=0;i<date.length;i++){
      var tmp={}
      tmp['Date']=date[i]
      tmp['Close']=data[i] 
      input.push(tmp)
    }
    var change=(closeLast-closeFirst)/closeFirst;
    if(change>0.3){
      var statement=" Oil price in "+years[0].toString()+" has skyrocketed by "+'<strong style="color: green;">'+
      (Math.abs(change).toFixed(4)*100).toString()+'%</strong>'
    } else if (change>0){
      var statement=" Oil price in "+years[0].toString()+" has increased by "+'<strong style="color: green;">'+
      (Math.abs(change).toFixed(4)*100).toString()+'%</strong>'
    }else if(change<-0.3){
      var statement=" Oil price in "+years[0].toString()+" has plummented by "+'<strong style="color: red;">'+
      (Math.abs(change).toFixed(4)*100).toString()+'%</strong>'
    }else{
      var statement=" Oil price in "+years[0].toString()+" has decreased by "+'<strong style="color: red;">'+
      (Math.abs(change).toFixed(4)*100).toString()+'%</strong>'
    }
    statement='<p><small><small>'+statement+'</small></small></p>'
    $('#statement').append(statement)
      //console.log(input)
      LineChartOil(input,$('#priceChartContainer').width(),$('#priceChartContainer').width()/2)
      var svg = d3.select("#oilPrice").append("svg")
      .attr("width", $('#priceChartContainer').width())
      .attr("height", $('#priceChartContainer').width()/2)
}
}); 

Deps.autorun(function () {
  var years=Session.get('lineChartDisplayYear')
  var negative=Session.get('negativeSentiment')
  var positive=Session.get('positiveSentiment')
  if(!negative){
    var monthNegativeNews={}
    var data2={}
    for(var i=0;i<years.length*12;i++){
    monthNegativeNews[i]=[];
    data2[i]=0
  }
    var sentimentData=[];
    var subscribe_handler_negative=Meteor.subscribe('oilPriceSentimentNews',false,[0,12*years.length],false,years);
    if (subscribe_handler_negative.ready()) {
     var  sentimentData = Collection.oilPriceSentimentNews.find({
      }).fetch();
    //  console.log(sentimentData)
    Session.set('negativeSentiment',true)
  }
  for(var i=0;i<sentimentData.length;i++){
    data2[new Date(sentimentData[i].publicationDate).getMonth()*(years.indexOf(new Date(sentimentData[i].publicationDate).getFullYear())+1)]+=1
    monthNegativeNews[new Date(sentimentData[i].publicationDate).getMonth()*(years.indexOf(new Date(sentimentData[i].publicationDate).getFullYear())+1)].push(sentimentData[i])
  }
  for(key in monthNegativeNews){
     // console.log(monthNegativeNews[key])
     if(monthNegativeNews.hasOwnProperty(key))
      monthNegativeNews[key].sort(function(a,b){
        return a['sentiment']-b['sentiment']
      })  
  }  
  Session.set('negativeNews',monthNegativeNews)
  var data=['negativeNews']
  for(var i=0;i<12*years.length;i++){
    data.push(data2[i])
  }
  var month=['x']
  for(var i=0;i<years.length;i++){
    var temp=['Jan '+years[i].toString(), 'Feb '+years[i].toString(), 'Mar '+years[i].toString(), 'Apr '+years[i].toString(), 'May '+years[i].toString(), 'Jun '+years[i].toString(), 'Jul '+years[i].toString(), 'Aug '+years[i].toString(), 'Sep '+years[i].toString(), 'Oct '+years[i].toString(), 'Nov '+years[i].toString(), 'Dec '+years[i].toString()]
    month=month.concat(temp)
  }
  sentimentChart.load({columns: [
    month,
    data
    ]});

}else{
  var sentimentData=[];
  var monthPositiveNews={}
    var data3={}
    for(var i=0;i<years.length*12;i++){
    monthPositiveNews[i]=[];
    data3[i]=0
  }
  var subscribe_handler_positive=Meteor.subscribe('oilPriceSentimentNews',true,[0,12*years.length],false,years);
  if (subscribe_handler_positive.ready()) {
    var sentimentData = Collection.oilPriceSentimentNews.find({
    }).fetch();
    //  console.log(sentimentData)
    Session.set('positiveSentiment',true)
    for(var i=0;i<sentimentData.length;i++){
      monthPositiveNews[new Date(sentimentData[i].publicationDate).getMonth()*(years.indexOf(new Date(sentimentData[i].publicationDate).getFullYear())+1)].push(sentimentData[i])
      data3[new Date(sentimentData[i].publicationDate).getMonth()*(years.indexOf(new Date(sentimentData[i].publicationDate).getFullYear())+1)]+=1
    }
    for(key in monthPositiveNews){
      if(monthPositiveNews.hasOwnProperty(key))
        monthPositiveNews[key].sort(function(a,b){
          return b['sentiment']-a['sentiment']
        })  
    } 
    Session.set('positiveNews',monthPositiveNews)


    var data=['positiveNews']
    for(var i=0;i<12*years.length;i++){
      data.push(data3[i])
    }
    sentimentChart.load({columns: [
      data
      ]});
  }
}



}); 

/*
Deps.autorun(function () {
  var subscribe_handler=Meteor.subscribe('oilImpactNews');
  if (subscribe_handler.ready()) {
    impact=Collection.oilImpactNews.find().fetch();
    tmp=[]
    for (var i=0;i<impact.length;i++){

      date= new Date(impact[i].publicationDate['$date'])
      date=moment(date).format('YYYY-MM-DD')
      if(tmp.indexOf(date)==-1){
        tmp.push(date)
      }
    }
    tmp.sort(function(a,b){
      return (new Date(a)-new Date(b))
    })
  //86400000= 1 day
  regions=[]
  
  for(var i=0;i<tmp.length;i++){
    var start= new Date(tmp[i])
    var last= new Date(tmp[i+1])
    if((last-start)===86400000){
      for(var j=i+2;j<tmp.length;j++){
        temp=new Date(tmp[j])
        if((temp-last)===86400000){
          last=temp
          if(j==tmp.length-1){
            result={};
            result['start']=moment(start).format('YYYY-MM-DD');
            result['end']=moment(last).format('YYYY-MM-DD');
            regions.push(result)
            i=j
            break;
          }
        }else{
          result={};
          result['start']=moment(start).format('YYYY-MM-DD');
          result['end']=moment(last).format('YYYY-MM-DD');
          regions.push(result)
          i=j
          break;
        }
        
      }
    }
  }
  ////console.log(regions)
  //priceChart.regions.add(regions)
}
});
*/

Deps.autorun(function () {
  document.getElementById('canvasWC').setAttribute('width',$('#wordContainer').width());
  document.getElementById('canvasWC').setAttribute('height',$('#wordContainer').width()/3);
/*
document.getElementById('canvasNegative').setAttribute('width',$('#wordCloudContainer').width());
document.getElementById('canvasNegative').setAttribute('height',$('#wordCloudContainer').width());
*/
if(Session.get('positiveSentiment')&&Session.get('negativeSentiment')){
  positive=Session.get('positiveNews')
  negative=Session.get('negativeNews')
  month=Session.get('newsSelectedMonth')
  var subscribe_handler=Meteor.subscribe('stopword')
  if (subscribe_handler.ready()) {
    stopword=Collection.stopword.find().fetch();
    if(Session.get('boolNews')){
      if(positive[month].length>0){
        tmp={}
        var tenTimesList=[]
        var tenTimesCount=[]
        for(var j=0;(j<10)&&(j<positive[month].length);j++){
          Stopword=stopword[0]['stopword']
          wordList=positive[month][j]['Full text'].match(/[a-zA-Z]+/g)
          Tmp={}
          tmpCount=0
          for(var i=0;i<wordList.length;i++){
            if(Stopword.indexOf(wordList[i])===-1){
              tmpCount+=1
              if (wordList[i] in tmp){
                tmp[wordList[i]]+=1
              }else{
                tmp[wordList[i]]=1
              }
              if (wordList[i] in Tmp){
                Tmp[wordList[i]]+=1
              }else{
                Tmp[wordList[i]]=1
              }
            }
          }
            for(val in Tmp){ 
              Tmp[val]=Tmp[val]/tmpCount
            }
            tenTimesList.push(Tmp)
          }
          var result=[]
          for (key in tmp){
            temp=[key,tmp[key]]
            result.push(temp)
          }
          result.sort(function(a,b){
            return b[1]*b[1].length-a[1]*a[1].length
          })
          var colorlist=['rgb(141,211,199)','rgb(255,255,179)','rgb(190,186,218)','rgb(251,128,114)','rgb(128,177,211)','rgb(253,180,98)','rgb(179,222,105)','rgb(252,205,229)','rgb(217,217,217)','rgb(188,128,189)']
          WordCloud(document.getElementById('canvasWC'), { backgroundColor:'#2B3E50',color:function(word, weight, fontSize, distance, theta){
            var dummy=0,Dummy=0;
            for(var i=0;i<tenTimesList.length;i++){
              if(word in tenTimesList[i]){
                if(tenTimesList[i][word]>Dummy){
                  dummy=i;
                  Dummy=tenTimesList[i][word];
                }
              }
            }
            return colorlist[dummy]
          }
            ,list: result,gridSize:5,weightFactor: 3,click: function(item) {
            dayLast=['31','28','31','30','31','30','31','31','30','31','30','31']
            var win = window.open('https://www.google.com/search?q=oil+price+'+item[0]+'&biw=1361&bih=733&source=lnt&tbs=cdr%3A1%2Ccd_min%3A'+'1'+'%2F'+(month+1).toString()+'%2F2014%2Ccd_max%3A'+dayLast[month]+'%2F'+(month+1).toString()+'%2F2014&tbm=nws', '_blank');
            win.focus();
  }} );
        }
      }else{

        if(negative[month].length>0){
          tmp={}
          var tenTimesList=[]
          var tenTimesCount=[]

          for(var j=0;(j<10)&&(j<negative[month].length);j++){
            Stopword=stopword[0]['stopword']
            wordList=negative[month][j]['Full text'].match(/[a-zA-Z]+/g)
            for(var i=0;i<wordList.length;i++){
              if(Stopword.indexOf(wordList[i])===-1)
                if (wordList[i] in tmp){
                  tmp[wordList[i]]+=1
                }else{
                  tmp[wordList[i]]=1
                }
              } 
            Tmp={}
          tmpCount=0
          for(var i=0;i<wordList.length;i++){
            if(Stopword.indexOf(wordList[i])===-1){
              tmpCount+=1
              if (wordList[i] in tmp){
                tmp[wordList[i]]+=1
              }else{
                tmp[wordList[i]]=1
              }
              if (wordList[i] in Tmp){
                Tmp[wordList[i]]+=1
              }else{
                Tmp[wordList[i]]=1
              }
            }
          }
            for(val in Tmp){ 
              Tmp[val]=Tmp[val]/tmpCount
            }
            tenTimesList.push(Tmp)
          }
            var result=[]
            for (key in tmp){
              temp=[key,tmp[key]]
              result.push(temp)
            }
            result.sort(function(a,b){
            return b[1]*b[1].length-a[1]*a[1].length
          })
            var colorlist=['rgb(141,211,199)','rgb(255,255,179)','rgb(190,186,218)','rgb(251,128,114)','rgb(128,177,211)','rgb(253,180,98)','rgb(179,222,105)','rgb(252,205,229)','rgb(217,217,217)','rgb(188,128,189)']
          WordCloud(document.getElementById('canvasWC'), { backgroundColor:'#2B3E50',color:function(word, weight, fontSize, distance, theta){
            var dummy=0,Dummy=0;
            for(var i=0;i<tenTimesList.length;i++){
              if(word in tenTimesList[i]){
                if(tenTimesList[i][word]>Dummy){
                  dummy=i;
                  Dummy=tenTimesList[i][word];
                }
              }
            }
            return colorlist[dummy]
          }
            ,list: result,gridSize:5,weightFactor: 3,click: function(item) {
            dayLast=['31','28','31','30','31','30','31','31','30','31','30','31']
            var win = window.open('https://www.google.com/search?q=oil+price+'+item[0]+'&biw=1361&bih=733&source=lnt&tbs=cdr%3A1%2Ccd_min%3A'+'1'+'%2F'+(month+1).toString()+'%2F2014%2Ccd_max%3A'+dayLast[month]+'%2F'+(month+1).toString()+'%2F2014&tbm=nws', '_blank');
            win.focus();
  }} );

          }
        }
      }
    }
  }); 

Deps.autorun(function () {
  document.getElementById('graphvis').setAttribute('width',$('#forceGraphContainer').width());
  document.getElementById('graphvis').setAttribute('height',$('#forceGraphContainer').width());
  var subscribe_handler_links=Meteor.subscribe('oilImpactNetwork')
  if(subscribe_handler_links.ready()){
    var links = Collection.oilImpactNetwork.find({}).fetch();

    var graphvis = new GraphVisOil("#graphvis",links,$("#forceGraphContainer").width(),$("#forceGraphContainer").width());
    graphvis.render();
    
  }
});

}
function getSortedKeys(obj) {
    var keys = []; for(var key in obj) keys.push(key);
    return keys.sort(function(a,b){return obj[b]-obj[a]});
}

function mergeSort(arr,sortingList)
{
  if (arr.length < 2)
    return arr;

  var middle = parseInt(arr.length / 2);
  var left   = arr.slice(0, middle);
  var right  = arr.slice(middle, arr.length);

  return merge(mergeSort(left,sortingList), mergeSort(right,sortingList),sortingList);
}

function merge(left, right,sortingList)
{
  var result = [];
  while (left.length && right.length) {
    if (sortingList[left[0]['name'].split(',')[0]] > sortingList[right[0]['name'].split(',')[0]])
    {
      result.push(left.shift());
    } 
    else if(sortingList[left[0]['name'].split(',')[0]] === sortingList[right[0]['name'].split(',')[0]])
    {
      if (sortingList[left[0]['name'].split(',')[1]] > sortingList[right[0]['name'].split(',')[1]])
      {
        result.push(left.shift());
      }else if (sortingList[left[0]['name'].split(',')[1]] === sortingList[right[0]['name'].split(',')[1]])
      {
        if (sortingList[left[0]['name'].split(',')[2]] >= sortingList[right[0]['name'].split(',')[2]]) 
        {
          result.push(left.shift());
        }else{
          result.push(right.shift());
        }
      }
      else{
        result.push(right.shift());
      }
    }
    else {
      result.push(right.shift());
    }
  }

  while (left.length)
    result.push(left.shift());

  while (right.length)
    result.push(right.shift());

  return result;
}

function findLineByLeastSquares(values_x, values_y) {
    var sum_x = 0;
    var sum_y = 0;
    var sum_xy = 0;
    var sum_xx = 0;
    var count = 0;

    /*
     * We'll use those variables for faster read/write access.
     */
    var x = 0;
    var y = 0;
    var values_length = values_x.length;

    if (values_length != values_y.length) {
        throw new Error('The parameters values_x and values_y need to have same size!');
    }

    /*
     * Nothing to do.
     */
    if (values_length === 0) {
        return [ [], [] ];
    }

    /*
     * Calculate the sum for each of the parts necessary.
     */
    for (var v = 0; v <values_length; v++) {
        x = values_x[v];
        y = values_y[v];
        sum_x += x;
        sum_y += y;
        sum_xx += x*x;
        sum_xy += x*y;
        count++;
    }

    /*
     * Calculate m and b for the formular:
     * y = x * m + b
     */
    var m = (count*sum_xy - sum_x*sum_y) / (count*sum_xx - sum_x*sum_x);
    var b = (sum_y/count) - (m*sum_x)/count;

    /*
     * We will make the x and y result line now
     */
    var result_values_x = [];
    var result_values_y = [];

    for (var v = 0; v < values_length; v++) {
        x = values_x[v];
        y = x * m + b;
        result_values_x.push(x);
        result_values_y.push(y);
    }

    return [result_values_x, result_values_y];
}

function remove(id) {
  return (elem=document.getElementById(id)).parentNode.removeChild(elem);
}

function emphasizeText(text) {

  var querystr = 'oil price';
  var reg = new RegExp(querystr, 'gi');
  var final_str = text.replace(reg, function(str) {return '<font color="red"><b>'+str+'</b></font>'});
  return final_str
}
function eventFire(el, etype){
  if (el.fireEvent) {
    (el.fireEvent('on' + etype));
  } else {
    var evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}
Template.mainview.helpers({
 'lda': function(){
  var result=[]
  for(var i=4;i<11;i++){
    var tmp={}
    tmp['index']=i
    result.push(tmp)
  }
  return result
},
'news': function() {
 var result=[]
 boolNews=Session.get('boolNews');
 if(boolNews){
  month=Session.get('newsSelectedMonth')
  positiveNews=Session.get('positiveNews')

  if(positiveNews[month].length>=5){

    for(var i=0;i<5;i++){
      var tmp={}
      tmp['title']=positiveNews[month][i]['Title']
      tmp['date']=positiveNews[month][i]['publicationDate']
      tmp['id']='positive'+i.toString()
      tmp['button']='Click here to see the Full Text'
      tmp['outcome']='#5CB85C'
      //tmp['relavance']=positiveNews[month][i]['sentiment']
      //console.log(tmp)
      result.push(tmp)
    }
    
  }
}else{
  var month=Session.get('newsSelectedMonth')
  var negativeNews=Session.get('negativeNews')
  if(negativeNews[month].length>=5){

    for(var i=0;i<5;i++){
      var tmp={}
      //console.log(negativeNews[month][i])
      tmp['title']=negativeNews[month][i]['Title']
      tmp['date']=negativeNews[month][i]['publicationDate']
      tmp['id']='negative'+i.toString()
      tmp['button']='Click here to see the Full Text'
      tmp['outcome']='#FF6763'
     // tmp['relavance']=negativeNews[month][i]['sentiment']
     //console.log(tmp)
     result.push(tmp)
   }
 }
}

Deps.afterFlush(function () {
  var pNews=Session.get('positiveNews')
  var month=Session.get('newsSelectedMonth')
  var nNews=Session.get('negativeNews')
  for(var i=0;i<5;i++){
    if($('#positive'+i.toString()).length){
      //console.log(news[month][0])
      var size=$('#newsContainer').width();
      $('#positive'+i.toString()).popover({

        html: true,
            //container: '.well',
            title: 'Full Text',
            placement: 'bottom',
            content:function(){
             return emphasizeText(pNews[month][i]['Full text'])
           },
           template: '<div class="popover special-class"><div class="arrow"></div><div class="popover-inner" style="overflow: auto; width:'+size.toString()+'px;height:'+size.toString()/2+'px;"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'
         })
      $('#positive'+i.toString()).attr('data-content', emphasizeText(pNews[month][i]['Full text']));
      var popover = $('#positive'+i.toString()).data('bs.popover');
      popover.setContent();
      popover.$tip.addClass(popover.options.placement);
    }
    if($('#negative'+i.toString()).length){

      var size=$('#newsContainer').width();
      $('#negative'+i.toString()).popover({

        html: true,
        trigger: 'manual',
        title: 'Full Text',
        placement: 'bottom',
        content:function(){
         return emphasizeText(nNews[month][i]['Full text'])
       },
       template: '<div class="popover special-class"><div class="arrow"></div><div class="popover-inner" style="overflow: auto; width:'+size.toString()+'px;height:'+size.toString()/2+'px;"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'
     })
      $('#negative'+i.toString()).attr('data-content', emphasizeText(nNews[month][i]['Full text']));
      var popover = $('#negative'+i.toString()).data('bs.popover');
      popover.setContent();
      popover.$tip.addClass(popover.options.placement);
    }
  }
});
return result
},

'newsMonth':function(){
  var bool=Session.get('boolNews')
  if(bool){
    var color='#5CB85C'
    var Bool="Positive"
  }else{
    var color='#D9534F'
    var Bool="Negative"
  }
  listofYear=[" "+Session.get('displayYear').toString()]
  listOfMonth=['January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  month=Session.get('newsSelectedMonth')
  return [{'month': listOfMonth[month]+listofYear[0],'bool':Bool,'color':color,'monthColor':'#80C0F5'}]
}
})



Template.mainview.events({
  'click #sentimentPositive': function(event) {
    var val = $(event.target).val();
    Session.set('boolNews',true)

  },
  'click #sentimentNegative': function(event) {
    var val = $(event.target).val();
    Session.set('boolNews',false)

  },
  'click #positive0': function(event) {

    $('#positive0').popover()
  },
  'click #positive1': function(event) {
   $('#positive1').popover()        
 },
 'click #positive2': function(event) {
   $('#positive2').popover()        
 },
 'click #positive3': function(event) {
   $('#positive3').popover()          
 },
 'click #positive4': function(event) {
   $('#positive4').popover()         
 },
 'click #negative0': function(event) {
   $('#negative0').popover() 
 },
 'click #negative1': function(event) {
   $('#negative1').popover() 
 },
 'click #negative2': function(event) {
   $('#negative2').popover() 
 },
 'click #negative3': function(event) {
   $('#negative3').popover()
 },
 'click #negative4': function(event) {
   $('#negative4').popover()
 },
});

Meteor.startup(function() {
  Session.set('lineChartDisplayYear',[2014])
  Session.set('displayYear',2014)
  Session.set('requestWord','None')
  Session.set('ldaList',[])
  Session.set('lda1',[])
  Session.set('lda2',[])
  Session.set('lda3',[])
  Session.set('lda4',[])
  Session.set('lda5',[])
  Session.set('lda6',[])
  Session.set('lda7',[])
  Session.set('lda8',[])
  Session.set('lda9',[])
  Session.set('lda10',[])
  Session.set('newsSelectedMonth',0);
  Session.set('boolNews',true)
  Session.set('negativeSentiment',false)
  Session.set('positiveSentiment',false)
  Session.set('positiveNews',{0:[],1:[],2:[],3:[],4:[],5:[],6:[],7:[],8:[],9:[],10:[],11:[]})
  Session.set('negativeNews',{0:[],1:[],2:[],3:[],4:[],5:[],6:[],7:[],8:[],9:[],10:[],11:[]})
})
