Factor: Interest Rate
Related Keywords: borrower, borrowers, lender, lender, default, inflation, mortgage, bank, Federal Reserve, loan, collateral, bond

Factor: Consumer Confidence
Related Keywords: business condition, employment, income, spending, nation’s economy, consumer confidence, saving, sale, retailer, inflation, business, 

Factor: Money Supply
Related Keywords: money supply, central bank, money, monetary policy, inflation, currency, traveler’s check, demand deposit, OCD, savings deposit, time deposit, ECB, China

Factor: Commodity
Related Keywords: gold, grain, oil, metal, natural gas, gas, beef, foreign currency,corn, CBOT, CME, DCE, GBOT, LIFFE, KCBT, MDEX, LME, NYMEX, NCEL, MCX, IIFCM, MATIF

Factor: Employment
Related Keywords:labor force, labor market, wage, salary, job, unemployment, labor, Fed, weekly hour

