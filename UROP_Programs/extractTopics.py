# -*- coding: utf-8 -*- 
import logging
from pymongo import MongoClient
import numpy as np
import re
from stop_words import get_stop_words
from collections import defaultdict
import itertools
from gensim import corpora
from gensim.models import LdaMulticore
from gensim.models.ldamodel import LdaModel
from gensim.models import TfidfModel
import pexpect

#child = pexpect.spawn('ssh gromit@vis.cse.ust.hk -f -L 27017:vis.cse.ust.hk:27017 -N')
#child.expect('password')
#child.sendline('87654321')

def extractTopics(indicator='interest rate',multicore=True,worker=3,num_topics=10,listOfDocument=None,importStopWords=[]):

    #client= MongoClient('localhost')
    

    #indicator='alibaba'
    #indicator='interest rate'
    #indicator='consumer confidence'
    #indicator='money supply'
    #indicator='commodity'
    #indicator='employment'

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)

    def clean_essay(string, lower=False):
        string = re.sub(r"\\t", " ", string)   
        string = re.sub(r"\\n", " ", string)   
        string = re.sub(r"\\r", " ", string)   
        string = re.sub(r"[^A-Za-z]", " ", string)  
        string = re.sub(r"\s{2,}", " ", string)
        if lower:
            string = string.lower()
        return string.strip()
    def add_plural(stop_words):
        for text in indicator.lower().split():
            if text[-1]=='y':
                stop_words.append(text[:-1]+"ies")
            else:
                stop_words.append(text+"s")
        return stop_words

    stop_words = get_stop_words('english')
    stop_words+=[u'u',u's',u'said',u'year',u't',u'mr',u'will',u'also',u'since',u'last',u'company',
                u'companies',u'according',u'can',u'inc',u'billion',u'million',u'one',
                    u'like',u'years',u'two',u'new',u'may',u'people',u'says',u'time',u'--',u'stock'
                    ,u'even',u'---',u'now',u'ms',u'besides']+importStopWords#+indicator.lower().split()
    stop_words= add_plural(stop_words)

    Stop_words=[]
    for words in stop_words:
        words = words[:1].upper() + words[1:]
        Stop_words.append(words)
    stop_words+=Stop_words
    

    if listOfDocument==None:
        db = client.UROP
        client = MongoClient('localhost',27017) #remember to SSH first ssh gromit@vis.cse.ust.hk -f -L 27017:vis.cse.ust.hk:27017 -N
        logger.info("Importing News")
        regex=re.compile(".*"+indicator+".*",re.IGNORECASE)
        cursor = db.financial_news_2014.find({"Title" : {"$regex":regex}},{"Full text":1})
        logger.info("Number of documents imported %d",cursor.count())
        documents=[]
        for document in cursor:
        	if document['Full text']:
        		temp= clean_essay(document['Full text'].encode('utf-8')).decode('utf-8')
        	documents.append(temp)
    else:
        documents=[]
        for document in listOfDocument:
            temp=clean_essay(document.encode('utf-8')).decode('utf-8')
            documents.append(temp)

    logger.info("Data Pre-processing: Bag of words")
    documents= ['' if v is None else v for v in documents]
    texts=[[word for word in document.split() if word not in stop_words] for document in documents]
    frequency = defaultdict(int)
    for text in texts:
    	for token in text:
    		frequency[token] += 1
    texts = [[token for token in text if frequency[token] > 1] for text in texts]
    dictionary = corpora.Dictionary(texts)
    corpus = [dictionary.doc2bow(text) for text in texts]

   # logger.info("Data Pre-processing: Tfidf")
   # tfidf = TfidfModel(corpus)
   # tfidf_corpus=[]
   # for document in corpus:
   #     tfidf_corpus.append(tfidf[document])


    logger.info("Generating topics from LDA")
    output=[]
    if multicore:
        model=LdaMulticore(num_topics=num_topics,workers=worker,corpus=corpus,id2word=dictionary,iterations=1500,batch=False)
    else:
        model=LdaModel(num_topics=num_topics,corpus=corpus,id2word=dictionary,iterations=1500,alpha='auto')

    for topic in model.print_topics(num_topics=100, num_words=3):
        result = ''.join([i for i in topic if not (i.isdigit() or (i=='*') or (i=='.')or (i=='+'))]).split()
        output.append(result)

    #logger.info("Calculate the Umass topic coherence for each topic and return the top topics")
    #top_topics= model.top_topics(num_words=5,corpus=corpus)
    #print "\n\n\nResults of topics from "+indicator
    
    #for idx,top_topic in enumerate(top_topics):
    #	print "Topic "+str(idx+1)+": "+" ".join([x[1] for x in top_topic[0]])
    #    temp=[x[1] for x in top_topic[0]]
     #   output.append(temp)
    return output