from extractTopics import *
from pymongo import MongoClient
from collections import defaultdict
import  matplotlib.pyplot as plt
import datetime
from datetime import datetime
import numpy as np
import pandas as pd
import re
date_list=np.arange('2014-01', '2015-01', dtype='datetime64[D]')
date_list=pd.DataFrame(date_list,columns=['Date'])
date_list['Date'].astype(datetime)
date_list['value']=0
datelist = pd.date_range(pd.datetime.today(), periods=365)
columns=["Indicator","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
rec=pd.DataFrame(columns=columns)
monthDict=['',"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec",'']
#indicators=['commodity','employment','interest rate','money supply','consumer confidence']
indicator='Oil'
#color={'money supply':'blue','interest rate':'red','consumer confidence':'green','commodity':'cyan','employment':'magenta'}
fig = plt.figure()
ax = plt.subplot(111)
lda=True
search=extractTopic(indicator=indicator,multicore=False,worker=3)
exit(0)
#for indicator in indicators:
result=pd.DataFrame(columns=['key','value','Date'])
for iteration in range(0,1):
	#timeFreq= defaultdict(lambda:0)
	for topics_list in search:
		#if lda:
		#	topics_list=extractTopic(indicator=indicator,multicore=False,worker=3)
			#query=[]
		#	for topics in topics_list:
		#		temp =''
		#		for topic in topics:
		#			temp += "(?=.*"+topic+".*)"
		temp =''
		for topic in topics_list:
			temp += "(?=.*"+topic+".*)"
			#query.append(temp)
			#regex= '|'.join(query)
		#else:
		#	regex="(?=.*"+indicator+".*)"
		#regex="(?=.*"+indicator+".*)"
		regex=temp
		print regex
		client= MongoClient('localhost',27017)
		db = client.UROP
		cursor = db.financial_news_2014.find({"Full text" : {"$regex":re.compile(regex,re.IGNORECASE)}},{"Publication date":1})
		if cursor.count()!=0:
			timeFreq=date_list.copy(deep=True)
			timeFreq['key']=','.join(topics_list)
			for document in cursor:
				if document['Publication date']:
				#print document['Publication date']
					#print timeFreq['value'][timeFreq['date']==document['Publication date']].values
					#print timeFreq['value'][timeFreq['date']==document['Publication date']].values[0]
					#exit(0)
					#print timeFreq['value'][timeFreq['date']==document['Publication date']].index[0]
					#exit(0)
					timeFreq.loc[timeFreq['value'][timeFreq['Date']==document['Publication date']].index[0],'value']+=1
			#print timeFreq
			result=pd.concat([result,timeFreq])
		#print result
		print "\nNumber of documents from topics "+str(cursor.count())
result['date']=result['Date'].apply(lambda x: x.strftime('%m/%d/%Y'))
result.drop('Date',inplace=True)
result.to_csv('result.csv',index=False)
	#sorted_by_date = sorted(timeFreq.items(), key=lambda (key,val): key)
	#xs,ys = zip(*sorted_by_date)
	#ax.plot(xs, ys, label=indicator)#,color=color[indicator])
	#temp=list(ys)
	#temp.insert(0,indicator)
	#rec=rec.append(pd.Series(temp,index=columns), ignore_index=True)

# Shrink current axis's height by 10% on the bottom
#box = ax.get_position()
#ax.set_position([box.x0, box.y0 + box.height * 0.1,
 #                box.width, box.height * 0.9])

# Put a legend below current axis
#ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
#          fancybox=True, shadow=True, ncol=3)
#plt.xticks(range(14),monthDict)
#plt.savefig('Popularity/popularity.png', format='png')
#rec.to_csv('Popularity/popularity.csv',index=False)