# -*- coding: utf-8 -*- 
from pymongo import MongoClient
import pandas as pd
from datetime import datetime
import time
from time import mktime
import copy
import re
import operator
from collections import OrderedDict
from stop_words import get_stop_words
from extractTopics import *
increaseList=['rebound','higher','rose','rise','climbed','rising','helped','supported',
			'heating','spike','support','boosted','jumped','climbing','pushed','boost',
			'jump','stronger','high','increase','increased','hike','hikes','increases',
			'surge','pushed','rises','gained','improving','improved','increasing','surging','soaring',
			'positive','strong','spur','pushing','soars','jumps','surges','supports','boosts',
			'boosting','recovered','recover','recovering','benefit']
decreaseList=['plunged','tumbled','tumbling','dropped','plunging','slump','fallen','plunge',
				'fell','slide','slides','sliding','low','fall','decline','drop','falling','lower','plummeting',
				'plummeted','collapse','collapsed','crash','falls','declining','declines']
impactList=['impact','benefiting','affect','effect','change','linked','effects','change','costs']
removeList=['s','in','the','and','in','to','as','are','that','on','will',
			'for','is','a','could','if','u','since','said','crude','global',
			'even','may','year','world','benchmark','markets','new','york',
			'amid','current','average','also','t','barrel','energy','hit',
			'per','recent','us','brent','international','oil','volatility',
			'information','service','however','around','now','despite','remain',
			'due','continue','likely','can','environment','term','last','cent',
			'june','market','break','sustained','years','sanctions','opec','russia','mr']
def clean_essay(string, lower=False):
        string = re.sub(r"\\t", " ", string)   
        string = re.sub(r"\\n", " ", string)   
        string = re.sub(r"\\r", " ", string)   
        string = re.sub(r"[^A-Za-z]", " ", string)  
        string = re.sub(r"\s{2,}", " ", string)
        if lower:
            string = string.lower()
        return string.strip()

def similarity(topic1,topic2):
	occurance=0.0
	for val in topic1:
		if (val in topic2):
			occurance+=1
	if((len(topic1)==0)and(len(topic2)==0)):
		occurance=0.0
	elif (len(topic1)==0):
		occurance=(float)(occurance/(float)(len(topic2)))
	elif (len(topic2)==0):
		occurance=(float)(occurance/(float)(len(topic1)))
	else:
		occurance=max((float)(occurance/(float)(len(topic2))),(float)(occurance/(float)(len(topic1))))	
	
	print occurance
	if (occurance>= 0.5):
		return True
	else:
		return False
client = MongoClient('localhost',3001)
db = client.meteor
regex=re.compile(".*"+'oil price'+".*",re.IGNORECASE)
end = datetime(2015, 1, 1, 0, 0, 0, 0)
cursor = db.oilPriceNews.find({"Full text" : {"$regex":regex},"publicationDate": {'$lt': end}},{'_id':0})
ldaList=[]
for document in cursor:
	if document['Full text']:
		isImpact=False
		temp=clean_essay(document['Full text'].lower()).split('oil price')
		for i, val in enumerate(temp[:-1]):
			tmp=temp[i].split()
			if len(tmp)>0:
				if len(tmp)>1:
					if len(tmp)>2:
						if tmp[-3] in impactList:
							isImpact=True
							break
					if tmp[-2] in impactList:
						isImpact=True
						break

				if tmp[-1] in impactList:
					isImpact=True
					break

			tmp=temp[i+1].split()
			if len(tmp)>0:
				if len(tmp)>1:
					if len(tmp)>2:
						if tmp[2] in impactList:
							isImpact=True
							break
					if tmp[1] in impactList:
						isImpact=True
						break

				if tmp[0] in impactList:
					isImpact=True
					break
		if isImpact:
			ldaList.append(document['Full text'])
importStopWords=[u'oil',u'price',u'prices',u'de',u'per',u'cent',u'les',u'la',u'des',u'-',u'et',u'net',
				u'gas',u'next',u'crude',u'said.',u'mr.',u'$',u'percent',u'investors',u'energy',u'barrel',
				u'à',u'du',u'en',u'le',u'une',u'que',u'expected',u'pays',u'pour',u'prix',u'countries',u'.',
				u'petrole',u'l',u'd',u'par',u'dans',u'p',u'many',u'b',u'o',u'seek',u'$',u'day',u'much',u'around',
				u'back',u'Friday',u'headed',u'area',u'q',u'bn',u'cents',u'week',u'see',u'including',u'month',u'long',
				u'un',u'sur']+impactList+decreaseList+increaseList+removeList
output=extractTopics(multicore=False,listOfDocument=ldaList,importStopWords=importStopWords,num_topics=100)
topicsDicts={}
topicList=[]
for topics in output:
	regex=''
	topic=topics[0]+'_'+topics[1]+'_'+topics[2]
	for words in topics:
		regex += "(?=.*"+words+".*)"
	cursor = db.oilPriceNews.find({"Full text" : {"$regex":re.compile(regex,re.IGNORECASE)},"publicationDate": {'$lt': end}},{'_id':0,'ProQuest document ID':1})
	tmp=[]
	for document in cursor:
		tmp.append(document['ProQuest document ID'])
	topicsDicts[topic]=tmp
	topicList.append(topic)
	print topic
matchList=[]
column={
	'source':None,
	'target':None,
	'type':'Non'
}
for i,val in enumerate(topicList[:-1]):
	j=i+1
	while j< len(topicList[:-1]):
		if similarity(topicsDicts[topicList[i]],topicsDicts[topicList[j]]):
			matchList.append([topicList[i],topicList[j]])
			print [topicList[i],topicList[j]]
			Temp=copy.deepcopy(column)
			Temp['source']=topicList[i]
			Temp['target']=topicList[j]
			print db.oilImpactNetwork.insert_one(Temp).inserted_id
		j+=1
for k,val in enumerate(topicList):
		Temp=copy.deepcopy(column)
		Temp['source']='oil price'
		Temp['target']=topicList[k]
		print db.oilImpactNetwork.insert_one(Temp).inserted_id
