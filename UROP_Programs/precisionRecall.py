# -*- coding: utf-8 -*- 
from pymongo import MongoClient
import re
from datetime import datetime
client = MongoClient('localhost',3001)
db = client.meteor
cursor=db.unemploymentSentimentNews.find()
GiantList=[]
for document in cursor:
	if(document['Subject']):
		GiantList+=document['Subject'].split(';')

regex1=re.compile(".*"+'Fuel oil prices'+".*",re.IGNORECASE)
regex2=re.compile(".*"+'Commodity prices'+".*",re.IGNORECASE)
regex3=re.compile(".*"+'Crude oil prices'+".*",re.IGNORECASE)
regex4=re.compile(".*"+'mployment'+".*",re.IGNORECASE)
month=[[1,31],[2,28],[3,31],[4,30],[5,31],[6,30],[7,31],[8,31],[9,30],[10,31],[11,30],[12,31]]
subject=[regex4]
regex=[]
for val in subject:
	regex.append({ 'Subject': {'$regex': val} })
doMonth=False
if doMonth:
	for val in month:
		print 'Month: '+str(val[0])
		start = datetime(2014, val[0], 1, 0, 0, 0, 0)
		end = datetime(2014, val[0], val[1], 0, 0, 0, 0)
		print 'Precision'
		for number in range(0,10):
			cursorDB=db.unemploymentSentimentNews.find({'$and':[{'$or':[{'sentiment':{'$gt':number}},{'sentiment':{'$lt':-1*number}}]},{"publicationDate": {'$lte': end}},{"publicationDate": {'$gte': start}}]})
			cursorDBMinus=db.unemploymentSentimentNews.find({'$and':[{'$or':[{'sentiment':{'$gt':number}},{'sentiment':{'$lt':-1*number}}]},{'Subject':None},{"publicationDate": {'$lte': end}},{"publicationDate": {'$gte': start}}]})
			cursor=db.unemploymentSentimentNews.find({ '$and':[{'$or':regex},
													{'$or':[{'sentiment':{'$gt':number}},{'sentiment':{'$lt':-1*number}}]},{"publicationDate": {'$lte': end}},{"publicationDate": {'$gte': start}}]})
			if(cursorDB.count()-cursorDBMinus.count()!=0):
				print (float)(cursor.count())/(float)(cursorDB.count()-cursorDBMinus.count())
			else:
				break
		print 'Recall'
		for number in range(0,10):
			cursorDB=db.unemploymentNews.find({'$and':[{'$or':regex},{"publicationDate": {'$lte': end}},{"publicationDate": {'$gte': start}}]})
			cursor=db.unemploymentSentimentNews.find({ '$and':[{'$or':regex},
													{'$or':[{'sentiment':{'$gt':number}},{'sentiment':{'$lt':-1*number}}]},{"publicationDate": {'$lte': end}},{"publicationDate": {'$gte': start}}]})
			if(cursorDB.count()!=0):
				print (float)(cursor.count())/(float)(cursorDB.count())
			else:
				break
		print

print 'Overall'
start = datetime(2014, 1, 1, 0, 0, 0, 0)
end = datetime(2014, 12, 31, 0, 0, 0, 0)
print 'Precision'
for number in range(0,10):
	cursorDB=db.unemploymentSentimentNews.find({'$and':[{'$or':[{'sentiment':{'$gt':number}},{'sentiment':{'$lt':-1*number}}]},{"publicationDate": {'$lte': end}},{"publicationDate": {'$gte': start}}]})
	cursorDBMinus=db.unemploymentSentimentNews.find({'$and':[{'$or':[{'sentiment':{'$gt':number}},{'sentiment':{'$lt':-1*number}}]},{'Subject':None},{"publicationDate": {'$lte': end}},{"publicationDate": {'$gte': start}}]})
	cursor=db.unemploymentSentimentNews.find({ '$and':[{'$or':regex},
											{'$or':[{'sentiment':{'$gt':number}},{'sentiment':{'$lt':-1*number}}]},{"publicationDate": {'$lte': end}},{"publicationDate": {'$gte': start}}]})
	if(cursorDB.count()-cursorDBMinus.count()!=0):
		print (float)(cursor.count())/(float)(cursorDB.count()-cursorDBMinus.count())
	else:
		break
print 'Recall'
for number in range(0,10):
	cursorDB=db.unemploymentNews.find({'$and':[{'$or':regex},{"publicationDate": {'$lte': end}},{"publicationDate": {'$gte': start}}]})
	cursor=db.unemploymentSentimentNews.find({ '$and':[{'$or':regex},
											{'$or':[{'sentiment':{'$gt':number}},{'sentiment':{'$lt':-1*number}}]},{"publicationDate": {'$lte': end}},{"publicationDate": {'$gte': start}}]})
	if(cursorDB.count()!=0):
		print (float)(cursor.count())/(float)(cursorDB.count())
	else:
		break
print