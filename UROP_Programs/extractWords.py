# -*- coding: utf-8 -*- 
from pymongo import MongoClient
import pandas as pd
from datetime import datetime
import time
from time import mktime
import copy
import re
import operator
from collections import OrderedDict
from stop_words import get_stop_words

increaseList=['rebound','higher','rose','rise','climbed','rising','helped','supported',
			'heating','spike','support','boosted','jumped','climbing','pushed','boost',
			'jump','stronger','high','increase','increased','hike','hikes','increases',
			'surge','pushed','rises','gained','improving','improved','increasing','surging','soaring',
			'positive','strong','spur','pushing','soars','jumps','surges','supports','boosts',
			'boosting','recovered','recover','recovering','benefit']
pastList=['rose','climbed','helped','supported','boosted','jumped','pushed','increased','surged','pushed',
			'gained','improved','soared','recovered','was','were','did','plunged','tumbled','tumbling','dropped',
			'fell','fallen','slided','collapsed']
decreaseList=['plunged','tumbled','tumbling','dropped','plunging','slump','fallen','plunge',
				'fell','slide','slides','sliding','low','fall','decline','drop','falling','lower','plummeting',
				'plummeted','collapse','collapsed','crash','falls','declining','declines']

impactList=['impact','benefiting','affect','effect','change','linked','effects','change','costs']

def clean_essay(string, lower=False):
        string = re.sub(r"\\t", " ", string)   
        string = re.sub(r"\\n", " ", string)   
        string = re.sub(r"\\r", " ", string)   
        string = re.sub(r"[^A-Za-z]", " ", string)  
        string = re.sub(r"\s{2,}", " ", string)
        if lower:
            string = string.lower()
        return string.strip()

client = MongoClient('localhost',3001)
db = client.meteor
regex=re.compile(".*"+'oil price'+".*",re.IGNORECASE)
end = datetime(2015, 1, 1, 0, 0, 0, 0)
cursor = db.oilPriceNews.find({"Full text" : {"$regex":regex},"publicationDate": {'$lt': end}},{'_id':0})
wordlist=[]
documents=[]
importList=[]
for document in cursor:
	if document['Full text']:
		Document={}
		isImpact=False
		temp=clean_essay(document['Full text'].lower()).split('oil price')
		#print temp
		score=0
		predictStance=0
		conclusionSense=0
		for i, val in enumerate(temp[:-1]):
			tmp=temp[i].split()
			#print tmp
			predictSentiment=False
			dummy=0
			if len(tmp)>0:
				if len(tmp)>1:
					if len(tmp)>2:
						wordlist.append(tmp[-3])
						#print tmp[-3]
						if tmp[-3] in increaseList:
							score+=1
							dummy+=1
						if tmp[-3] in decreaseList:
							score-=1
							dummy-=1
						if tmp[-3] == 'impact':
							isImpact=True
						if tmp[-3] =='will':
							predictSentiment =True


					wordlist.append(tmp[-2])
					#print tmp[-2]
					if tmp[-2] in increaseList:
						score+=1
						dummy+=1
					if tmp[-2] in decreaseList:
						score-=1
						dummy-=1
					if tmp[-2] == 'impact':
						isImpact=True
					if tmp[-2] =='will':
						predictSentiment =True

				wordlist.append(tmp[-1])
				#print tmp[-1]
				if tmp[-1] in increaseList:
					score+=1
					dummy+=1
				if tmp[-1] in decreaseList:
					score-=1
					dummy-=1
				if tmp[-1] == 'impact':
					isImpact=True
				if tmp[-1] =='will':
					predictSentiment =True

			tmp=temp[i+1].split()
			if len(tmp)>0:
				if len(tmp)>1:
					if len(tmp)>2:
						wordlist.append(tmp[2])
						#print tmp[2]
						if tmp[2] in increaseList:
							score+=1
							dummy+=1
						if tmp[2] in decreaseList:
							score-=1
							dummy-=1
						if tmp[2] == 'impact':
							isImpact=True
						if tmp[2] =='will':
							predictSentiment =True

					wordlist.append(tmp[1])
					#print tmp[1]
					if tmp[1] in increaseList:
							score+=1
							dummy+=1
					if tmp[1] in decreaseList:
							score-=1
							dummy-=1
					if tmp[1] == 'impact':
						isImpact=True
					if tmp[1] =='will':
						predictSentiment =True

				wordlist.append(tmp[0])
				#print tmp[0]
				if tmp[0] in increaseList:
						score+=1
						dummy+=1
				if tmp[0] in decreaseList:
						score-=1
						dummy-=1
				if tmp[0] == 'impact':
					isImpact=True
				if tmp[0] =='will':
					predictSentiment =True

			if predictSentiment:
				predictStance+=dummy
		if score>0:
			#print document['Title'].replace(u'\xa0', u'')
			#print 'increase'
			document['sentiment']=score
			document['isImpact']=isImpact
			document['predictStance']=predictStance
			importList.append(document)

		if score<0:
			#print document['Title'].replace(u'\xa0', u'')
			#print 'decrease'
			document['sentiment']=score
			document['isImpact']=isImpact
			document['predictStance']=predictStance
			importList.append(document)


		if ((score==0) and (isImpact)):
			document['sentiment']=score
			document['isImpact']=isImpact
			document['predictStance']=predictStance
			importList.append(document)

#result = db.oilPriceSentimentNews.insert_many(importList)
#print result.inserted_ids


#exit(0)
result={}
for val in wordlist:
	if val in result:
		result[val]+=1
	else:
		result[val]=1
result=OrderedDict(sorted(result.items(), key=lambda t: t[1]))
removeList=['s','in','the','and','in','to','as','are','that','on','will',
			'for','is','a','could','if','u','since','said','crude','global',
			'even','may','year','world','benchmark','markets','new','york',
			'amid','current','average','also','t','barrel','energy','hit',
			'per','recent','us','brent','international','oil','volatility',
			'information','service','however','around','now','despite','remain',
			'due','continue','likely','can','environment','term','last','cent',
			'june','market','break','sustained','years','sanctions','opec','russia','mr']
stop_words = get_stop_words('english')
for val in list(set(removeList) | set(stop_words)):
	if val in result:
		result.pop(val,None)
for key in result:
	print str(key)+": "+str(result[key])
		

	#documents.append(temp)