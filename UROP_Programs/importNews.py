from pymongo import MongoClient
import os
from datetime import datetime
import re
import copy
import time
from time import mktime



directory="articles/oil price"						#directory
client = MongoClient('localhost',3001)
db = client.meteor 													#database name
posts = db.oilPriceNews									#collection name


#parse files to list
articles = []
corpus=[]
category={
	'Author':None,
	'Abstract':None,
	'Links':None,
	'Full text':None,
	'Credit':None,
	'Subject':None,
	'Location':None,
	'Company / organization':None,
	'Title':None,
	'Publication title':None,
	'Pages':None,
	'Publication year':None,
	'publicationDate':None,
	'Year':None,
	'Section':None,
	'Publisher':None,
	'Place of publication':None,
	'Country of publication':None,
	'Publication subject':None,
	'Source type':None,
	'Language of publication':None,
	'Document type':None,
	'ProQuest document ID':None,
	'Document URL':None,
	'Copyright':None,
	'Last updated':None,
	'Database':None
		}
for file in os.listdir(directory):
	if file.endswith(".txt"):
		articles.append(file)
for file in articles:
	#print file
	f= open(directory+"/"+file, "r");
	file_list = f.readlines()
	file_list = [item.rstrip() for item in file_list]
	file_list=filter(lambda a: a != '', file_list)
	article_start= [i for i, x in enumerate(file_list) if x == file_list[0]]
	for breakline in range(0,len(article_start)-1):
		news= file_list[(article_start[breakline]+1):article_start[breakline+1]]
		#print news
		temp=copy.deepcopy(category)
		for val in news:
			for key in category:
				if key in val:
					temp[key]=val.strip(key+":").lstrip()
					if (key=='Full text') or (key=='Abstract'):
						index=0
						index= news.index(val)+1
						isText=True
						while isText:
							for key_2 in category:
								try:
									if (key_2+":") in news[index]:
										isText=False
								except IndexError:
									isText=False
									break
							if isText:
								temp[key]= temp[key]+'\n'+news[index]
								index+=1
					if (key=='publicationDate'):
						#print temp[key]
						if('-' in temp[key]):
							temp[key]=temp[key].split('-')[0]+','+temp[key].split('-')[1].split(',')[1]
						temp[key]=datetime.fromtimestamp(mktime(time.strptime(temp[key], '%b %d, %Y')))
					break
		corpus.append(copy.deepcopy(temp))
	f.close()
# Remove Duplicates
seen = set()
news_corpus=[]
for d in corpus:
	t = tuple(d.items())
	if t not in seen:
		seen.add(t)
		news_corpus.append(d)

result = posts.insert_many(news_corpus)
print result.inserted_ids
#print sorted(filter(lambda x: x!=None,[d['Publication date'] for d in news_corpus]))