# -*- coding: utf-8 -*- 
from pymongo import MongoClient
from Wordcloud.wordcloud import WordCloud, STOPWORDS
import numpy as np
from scipy.misc import imread
from random import Random
import datetime
from dateutil import parser
import re

color1=360
color2=25
color3=56
color4=100
color5=175
color6=240
color7=287
def color_func(word=None, font_size=None,position=None, orientation=None,random_state=None, font_path=None):
	if word != None:
		if word[1]==1:
			color=color1
		elif word[1]>=0.9:
			color=color2
		elif word[1]>=0.8:
			color=color3
		elif word[1]>=0.7:
			color=color4
		elif word[1]>=0.6:
			color=color5
		elif word[1]>=0.5:
			color=color6
		elif word[1] >=0.4:
			color=color7
		else:
			return "hsl(%d, 80%%, 80%%)" % color7

	return "hsl(%d, 80%%, 50%%)" % color

if __name__=="__main__":
	#print STOPWORDS
	stopwordlist=set(["said","de","U","u","will","mr","Mr.","s","year","new","one","say","also","time","says","la"])
	stopwordlist |= STOPWORDS
	client = MongoClient('Gromits-MacBook-Pro.local', 27000)
	db = client.UROP
	indicators=['interest rate', 'consumer confidence', 'money supply','commodity','employment']
	


	#stopwordlist |= set(['percent','us','oil',"energy",'market','price','prices','united','states'])

	circle_mask = imread("circle.png")
	wc =WordCloud(background_color="white",stopwords=stopwordlist,mask=circle_mask,max_words=1000,ranks_only=True,min_font_size=8)

	for indicator in indicators:
		regex=re.compile(".*"+indicator+".*")
		cursor = db.financial_news_2014.find({"Full text" : {"$regex":regex}},{"Full text":1})
		words=[]
		for document in cursor:
			words.append(document['Full text'])
		words= ['' if v is None else v for v in words]
		rawnews=" ".join(words).encode('ascii', 'ignore').decode().encode('utf-8')
		wordcloud = wc.generate(rawnews).recolor(color_func=color_func)
		wordcloud.to_file(indicator+"_title.png")
	